/*
 * xprofile.h
 *
 * created on: Mar 23, 2018
 * author: aasthakm
 *
 * xen local profile datastructure
 */

#ifndef __XPROFILE_H__
#define __XPROFILE_H__

#include "sme_guest.h"
#include "sme_hashtable.h"
#include <xen/spinlock.h>

#define DEFAULT_PROFILE_ID 100

#define XP_NOENT  1
#define XP_INVAL  2
#define XP_NOCHG  3
#define XP_EXPRD  4
#define XP_COMPL  5
#define XP_PAUSE  6
#define XP_UINIT  7
#define XP_REPLC  8

enum {
  PKT_REAL = 0,
  PKT_DUMMY,
};

/*
 * element in xen's profile list
 */
typedef struct xprofile {
  /*
   * current profile id of the profile
   */
  ihash_t prof_id;
  conn_t conn_id;
  /*
   * req TS as in the shared profile structure
   */
  uint64_t req_ts;
  /*
   * convert to s_time for xen.
   * get_s_time_fixed returns diff. values on same input at diff. times,
   * possibly due to varying cycle count values on different cores.
   * profile may be set via a hypercall which may occur on a core different
   * from the one where we usually use the timestamps during enforcement.
   */
  s_time_t req_stime;
  /*
   * last timestamp when the profile was paused due to cwnd closing.
   * debug-only variable
   */
  s_time_t last_pause_stime;
  uint16_t id:13, // unused
           pid_replace_status:3;
  int x_real_count;
  int x_dummy_count;
  /*
   * index of first dummy pkt
   */
  int first_dummy_idx;
  /*
   * index of last dummy pkt
   */
  int last_real_idx;
  /*
   * latency shift to apply when next_timer_idx == cwnd_shift_idx
   */
  uint64_t cwnd_shift_val;
  uint16_t cwnd_shift_idx;
#if CONFIG_XPROF_TIMER_ARRAY
  /*
   * instantiated timers for the profile
   */
  uint64_t *timers;
#endif
  /*
   * pointer to profile in pmap_htbl
   */
  profile_t local_profile;
  /*
   * link into the linked list
   */
  list_t xprof_listp;
  /*
   * spinlock for timers array and next_timer_idx
   */
  spinlock_t timers_lock;
} xprofile_t;

enum {
  PIDR_UNTESTED = 0,
  PIDR_REPLACED,
  PIDR_UNCHANGED,
  PIDR_REJECTED,
  MAX_PIDR_STATUS
};

void init_ihashes(void);
void init_profiles(void *htable);
int init_profiles_from_buf(sme_htable_t *pmap_htbl, char *buf, int buf_len);
void cleanup_profile(profile_t *p);
void clone_profile(profile_t *src_p, profile_t *dst_p);

uint64_t get_latency_for_req(profile_t *p, int req_idx);

void print_profile(profile_t *p);
int get_req_frame_idx_from_timer_idx(profile_t *p, uint16_t timer_idx,
    int *req_idx, int *frame_idx);

void init_xprofile_timers(xprofile_t *xprof, profq_elem_t *gprof, profile_t *prof);
void init_xprofile(xprofile_t *xprof, profq_elem_t *gprof, sme_htable_t *pmap_htbl);
int xprofile_pick_next_ts(xprofile_t *xprof, profq_elem_t *gprof, uint64_t *ts);
int64_t xprofile_get_timer_at_idx(xprofile_t *xprof, profq_elem_t *gprof,
    uint16_t timer_idx);

int replace_xprofile(xprofile_t *xprof, profq_elem_t *prof,
    sme_htable_t *pmap_htbl, int prof_extn, s_time_t deadline);
void reset_xprofile_unlocked(xprofile_t *xprof, profq_elem_t *gprof,
    sme_htable_t *pmap_htbl, int replace_status);
int extend_xprofile_unlocked(xprofile_t *xprof, profq_elem_t *gprof);

void cleanup_xprofile_unlocked(xprofile_t *xprof);
int xprofile_pick_next_ts_unlocked(xprofile_t *xprof, profq_elem_t *gprof,
    uint64_t *ts);
int xprofile_get_prev_ts_unlocked(xprofile_t *xprof, profq_elem_t *gprof,
    uint64_t *ts);

void xprofile_update_packet_counter(xprofile_t *xprof, int value, int type);
void xprofile_update_gprof_packet_counter(xprofile_t *xprof, profq_elem_t *gprof,
    int value, int type, int from_xprof);

static inline int is_timeslot_expired(s_time_t slot, s_time_t deadline)
{
  if (deadline != 0) // for epoch based design
    return (slot < deadline);
  else // for event driven design
    return (NOW() >= slot);
}

#endif /* __XPROFILE_H__ */
