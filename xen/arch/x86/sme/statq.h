/*
 * statq.h
 *
 * created on: Jan 20, 2018
 * author: aasthakm
 *
 * queue to hold timeseries data to be printed at rmmod
 */

#ifndef __STATQ_H__
#define __STATQ_H__

#include <xen/lib.h>
#include <asm/system.h>
#include "xconfig.h"

typedef struct statq_elem {
  u64 start_ts;
  u32 latency;
} statq_elem_t;

typedef struct statq {
  u32 size;
  u32 prod_idx;
  u32 cons_idx;
  statq_elem_t *arr;
} statq_t;

int init_statq(statq_t *statq, int size);
void cleanup_statq(statq_t *statq);
int put_statq(statq_t *statq, statq_elem_t *v);
int get_statq(statq_t *statq, statq_elem_t *v);
void print_statq(statq_t *statq);

static inline int statq_empty(statq_t *statq)
{
  return (statq->cons_idx == statq->prod_idx);
}

static inline int statq_full(statq_t *statq)
{
  return ((statq->prod_idx % statq->size == statq->cons_idx % statq->size)
      && (statq->prod_idx / statq->size != statq->cons_idx / statq->size));
}

typedef struct generic_q {
  uint64_t prod_idx;
  uint64_t cons_idx;
  int qidx;
  int size;
  int elem_size;
  uint16_t port;
  uint16_t has_negative;
  void *elem_arr;
  void (*printfn)(struct generic_q *queue);
} generic_q_t;

#define init_generic_q(Q, i, s, t, f)                 \
{                                                     \
  (Q).elem_arr = (t *) xzalloc_array(t, s);           \
  (Q).qidx = i;                                       \
  (Q).size = s;                                       \
  (Q).elem_size = sizeof(t);                          \
  (Q).prod_idx = 0;                                   \
  (Q).cons_idx = 0;                                   \
  (Q).printfn = f;                                    \
}

#define cleanup_generic_q(Q)  \
{                             \
  if ((Q).elem_arr) {         \
    xfree((Q).elem_arr);      \
    (Q).elem_arr = NULL;      \
    (Q).elem_size = 0;        \
    (Q).size = 0;             \
    (Q).prod_idx = 0;         \
    (Q).cons_idx = 0;         \
  }                           \
}

#define print_generic_q(Q)  \
{                           \
  if ((Q).printfn) {        \
    (Q).printfn(&(Q));      \
  }                         \
}

static inline int generic_q_empty(generic_q_t *queue)
{
  return (queue->cons_idx == queue->prod_idx);
}

static inline int generic_q_full(generic_q_t *queue)
{
  return ((queue->prod_idx % queue->size == queue->cons_idx % queue->size)
      && (queue->prod_idx / queue->size != queue->cons_idx / queue->size));
}

static inline void put_generic_q(generic_q_t *queue, void *elem)
{
  void *e = NULL;
  int ret = 0;
  uint64_t curr_prod_idx = 0;
  if (!generic_q_full(queue)) {
    do {
      curr_prod_idx = queue->prod_idx;
      ret = cmpxchg(&queue->prod_idx, curr_prod_idx, curr_prod_idx+1);
    } while (ret != curr_prod_idx);

    curr_prod_idx = curr_prod_idx % queue->size;
    e = (void *) ((unsigned long) queue->elem_arr
        + (curr_prod_idx * queue->elem_size));
    memcpy(e, elem, queue->elem_size);
  }
}

typedef struct global_pkt_elem {
  uint32_t port;
  uint32_t seqno;
  uint32_t seqack;
  uint32_t cwm;
  uint16_t db_val;
  uint16_t bd_cons;
  uint16_t guest_prof_slot_idx;
  uint16_t timer_idx;
  uint16_t real_cnt;
  uint16_t dummy_cnt;
  uint16_t num_timers;
} global_pkt_elem_t;
extern generic_q_t global_pkt_q;
extern void do_print_global_pkt_q(generic_q_t *queue);


typedef struct fine_stat_elem {
  uint32_t epoch_id;
  uint32_t wakeup_delay;
  uint32_t db_delay;
  uint32_t free_delay;
  uint32_t dequeue_delay;
  uint32_t prep_delay;
  uint16_t n_free;
  uint16_t n_dequeue;
  uint16_t n_prep;
} fine_stat_elem_t;
extern void do_print_fine_stat_q(generic_q_t *queue);

typedef struct spurious_elem {
  uint64_t epoch_id;
  uint64_t wakeup_delay;
  uint64_t db_delay;
  uint64_t prep_delay;
  uint64_t last_delay;
  int16_t n_prep;
  int16_t n_other;
  int16_t n_ev1;
  int16_t n_ev2;
  int16_t n_ev3;
  int16_t n_ev4;
  uint8_t overflow:1,
          db:1,
          n_pkt:3,
          pad:3;
} spurious_elem_t;
extern generic_q_t spurious_q;
extern void do_print_spurious_q(generic_q_t *queue);

typedef struct nicstall_elem {
  uint64_t start_time;
  uint64_t end_time;
  uint16_t start_bd;
  uint16_t start_pkt;
  uint16_t start_prep;
} nicstall_elem_t;
extern generic_q_t nicstall_q;
extern void do_print_nicstall_q(generic_q_t *queue);

typedef struct pkts_elem {
  uint64_t ts;
  uint32_t port;
  uint32_t seqno;
  uint32_t seqack;
  uint16_t ip_id;
  uint16_t db;
  uint16_t pkt_prod;
  uint16_t pkt_cons;
  uint16_t pkt_type;
} pkts_elem_t;
extern void do_print_pkts_q(generic_q_t *queue);

#endif /* __STATQ_H__  */
