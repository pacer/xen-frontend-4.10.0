#include "xdebug.h"
#include "xconfig.h"
#include "openssl_wrap.h"

int init_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p,
    ctr_state_t *e_state, ctr_state_t *d_state,
    unsigned char *key, unsigned char *iv)
{
  int ret = 0;
  EVP_CIPHER_CTX *e_ctx = NULL;
  EVP_CIPHER_CTX *d_ctx = NULL;

  if (!(e_ctx = EVP_CIPHER_CTX_new())) {
    ret = -ENOMEM;
    goto err;
  }

  if (!(d_ctx = EVP_CIPHER_CTX_new())) {
    ret = -ENOMEM;
    goto err;
  }

  memset(e_ctx, 0, sizeof(EVP_CIPHER_CTX));
  memset(d_ctx, 0, sizeof(EVP_CIPHER_CTX));

  if (1 != EVP_EncryptInit_ex(e_ctx, EVP_aes_256_ctr(), NULL, NULL, NULL)) {
    ret = -EINVAL;
    goto err;
  }

  if (1 != EVP_DecryptInit_ex(d_ctx, EVP_aes_256_ctr(), NULL, NULL, NULL)) {
    ret = -EINVAL;
    goto err;
  }

  init_ctr(e_state, key, iv);
  init_ctr(d_state, key, iv);

  *e_ctx_p = e_ctx;
  *d_ctx_p = d_ctx;
  return 0;

err:
  if (e_ctx) {
    EVP_CIPHER_CTX_free(e_ctx);
  }
  if (d_ctx) {
    EVP_CIPHER_CTX_free(d_ctx);
  }

  return -EINVAL;
}

void reset_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p)
{
  EVP_CIPHER_CTX_reset_shallow(*e_ctx_p);
  EVP_CIPHER_CTX_reset_shallow(*d_ctx_p);
}

void free_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p)
{
  EVP_CIPHER_CTX_free(*e_ctx_p);
  EVP_CIPHER_CTX_free(*d_ctx_p);
}

int do_openssl_enc(EVP_CIPHER_CTX *e_ctx, ctr_state_t *state,
    unsigned char *plaintext, int plaintext_len, unsigned char *ciphertext)
{
  int len;
  int ciphertext_len;

  if (1 != EVP_EncryptInit_ex(e_ctx, NULL, NULL, state->key, state->iv))
    return -EINVAL;

  if (1 != EVP_EncryptUpdate(e_ctx, ciphertext, &len, plaintext, plaintext_len))
    return -EINVAL;

  ciphertext_len = len;

  if (1 != EVP_EncryptFinal_ex(e_ctx, ciphertext + len, &len))
    return -EINVAL;

  ciphertext_len += len;

  return ciphertext_len;
}

int do_openssl_dec(EVP_CIPHER_CTX *d_ctx, ctr_state_t *state,
    unsigned char *ciphertext, int ciphertext_len, unsigned char *plaintext)
{
  int len, plaintext_len;

  if (1 != EVP_DecryptInit_ex(d_ctx, NULL, NULL, state->key, state->iv))
    return -EINVAL;

  if (1 != EVP_DecryptUpdate(d_ctx, plaintext, &len, ciphertext, ciphertext_len))
    return -EINVAL;

  plaintext_len = len;

  if (1 != EVP_DecryptFinal_ex(d_ctx, plaintext + len, &len))
    return -EINVAL;

  plaintext_len += len;

  return plaintext_len;
}
