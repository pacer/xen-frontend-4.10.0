/*
 * sme_hashtable.c
 *
 * created on: Nov 8, 2017
 * author: aasthakm
 *
 * hashtable for pcm lookup data structure
 */

#include "xdebug.h"
#include "sme_hashtable.h"

int
init_htable(sme_htable_t *htbl, int size, hash_fn_t *hfn)
{
  int i;
  if (!htbl || !size || !hfn)
    return -EINVAL;

  htbl->ht_list = xzalloc_array(list_t, size);
  if (!htbl->ht_list) {
    return -ENOMEM;
  }

  for (i = 0; i < size; i++) {
    INIT_LIST_HEAD(&htbl->ht_list[i]);
  }

  htbl->size = size;
  htbl->ht_func = hfn;
  xprintk(LVL_INFO, "Init PCM HT of size: %d", htbl->size);
  return 0;
}

void
cleanup_htable(sme_htable_t *htbl)
{
  int i;
  if (!htbl->ht_func && !htbl->ht_list && !htbl->size)
    return;

  if (htbl->ht_list) {
    for (i = 0; i < htbl->size; i++) {
      htbl->ht_func->free(&htbl->ht_list[i]);
    }

    xfree(htbl->ht_list);
  }

  xprintk(LVL_INFO, "%s", "Cleanup PCM HT");
}

int
htable_key_idx(sme_htable_t *htbl, void *key, int keylen)
{
  uint32_t idx = htbl->ht_func->hash(key, keylen, 0);
  int idx2 = idx & (htbl->size-1);
  return idx2;
}

void
htable_insert(sme_htable_t *htbl, void *key, int keylen, list_t *elem_listp)
{
  int idx;

  if (!htbl || !key || !keylen || !elem_listp)
    return;

  idx = htable_key_idx(htbl, key, keylen);
  if (idx < 0)
    return;

  list_add_tail(elem_listp, &htbl->ht_list[idx]);
}

void *
htable_lookup(sme_htable_t *htbl, void *key, int keylen)
{
  void *entry = NULL;
  list_t *ht_list;
  int idx, ret;

  idx = htable_key_idx(htbl, key, keylen);
  ht_list = &htbl->ht_list[idx];
  ret = htbl->ht_func->lookup(ht_list, key, keylen, &entry);

  if (!ret)
    return entry;

  return NULL;
}
