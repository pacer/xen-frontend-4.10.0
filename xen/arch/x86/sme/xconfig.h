#ifndef __XCONFIG_H__
#define __XCONFIG_H__

#include <xen/lib.h>

extern uint32_t PMAP_HTBL_SIZE;
extern uint32_t PAC_TIMERQ_SIZE;
extern uint32_t NIC_DATA_QSIZE;
extern uint32_t CWND_UPDATE_LATENCY;
extern uint32_t REXMIT_UPDATE_LATENCY;
extern uint32_t GLOBAL_NIC_DATA_QSIZE;
extern uint32_t PACER_SPIN_THRESHOLD;
extern uint32_t XEN_PACER_CORE;
extern uint32_t HYPACE_CFG;
extern uint32_t EPOCH_SIZE;
extern uint32_t PTIMER_HEAP_SIZE;
extern uint32_t MAX_DEQUEUE_TIME;
extern uint32_t MAX_PROF_FREE_TIME;
extern int TCP_MTU;
extern int CONFIG_SW_CSUM;
extern int MAX_HP_PER_IRQ;
extern int MAX_PKTS_PER_EPOCH;
extern int MAX_OTHER_PER_EPOCH;
extern int N_FREE;
extern int N_DEQUEUE;
extern int N_PREP;
extern int N_OTHER;
extern int N_PKT;
extern int N_DB;
extern s64 ALLOWANCE;


/*
 * =============================================
 * config for guest-hv synchronization mechanism
 * =============================================
 */
#define SYNC_LOCKED   0
#define SYNC_LOCKFREE 1
#define CONFIG_SYNC   SYNC_LOCKFREE

/*
 * ====================================
 * config for profile freeing mechanism
 * ====================================
 */
// profile directly freed by xen after lost time slot expiry
#define PROF_FREE_XEN 0
// tcp notifies to free a profile slot after all pkts are acked
#define PROF_FREE_TCP 1
#define CONFIG_PROF_FREE  PROF_FREE_TCP

/*
 * ==================================
 * config for dummy packet generation
 * ==================================
 */
#define DUMMY_NOACK   0
#define DUMMY_OOB     1
#define CONFIG_DUMMY  DUMMY_OOB

/*
 * =============================
 * config for per-flow nic queue
 * =============================
 */
#define CONFIG_NQ_STATIC  0
#define CONFIG_NQ_DYNAMIC 1
#define CONFIG_NIC_QUEUE  CONFIG_NQ_STATIC

/*
 * =============
 * hypace config
 * =============
 */
#define HP_ORIG         0
#define HP_BATCH        1
#define HP_BATCH2       2
#define HP_BATCH3       3

/*
 * theoretical max # of hypace cores
 * limits size of the statically defined arrays
 */
#define MAX_HP_ARGS     8

/*
 * =======================
 * profile related configs
 * =======================
 */
#define CONFIG_XPROF_TIMER_ARRAY 0

/*
 * should always be 1. 0 is being used for
 * debugging the memory corruption bug.
 */
#define CONFIG_ALLOC_LOCAL_PROF 1

#define CONFIG_XEN_PACER_SLOTSHARE  0

/*
 * config to disable lock synchronization on timerq and active_timerq
 * 0 -- disables the locks
 */
#define CONFIG_TIMERQ_SPINLOCK  0
/*
 * config to disable lock synchronization on ptimer heap
 * 0 -- disables the locks
 */
#define CONFIG_HEAP_SPINLOCK    0
/*
 * even though no lock synchronization, disable IRQs around critical
 * section to prevent hypace from interrupting itself upon time overflow.
 * not entirely clear if this needed, but I believe we do.
 */
#define CONFIG_SAVE_IRQ 1

/*
 * config to enable mbedtls encryption operation
 */
#define CRYPTO_NONE     0
#define CRYPTO_MBEDTLS  1
#define CRYPTO_OPENSSL  2

#define CONFIG_PACER_CRYPTO 2

/*
 * Ethernet header:                         14 bytes
 * IP header:                               20 bytes
 * TCP header src port:                      2 bytes
 * TCP header dst port:                      2 bytes
 * TCP header seqno:                         4 bytes
 * Total header to exclude from encryption: 42 bytes
 */
#define MANGLE_START_OFFSET 42

/*
 * MTU:                   1514 bytes
 * header excluded:         42 bytes
 * encryption buffer len: 1472 bytes
 */
#define ENC_BUF_SIZE	1472

/*
 * =====================
 * config for statistics
 * =====================
 */
#define SME_CONFIG_STAT 1
#define SME_CONFIG_STAT_DETAIL 1
#define HYPACE_CONFIG_STAT 1

#define SCALE_NS    0
#define SCALE_RDTSC 1

#define SME_CONFIG_STAT_SCALE SCALE_RDTSC

#define TSC_TO_NS_SCALE 3200

#define SPURIOUS_EVENTS_QSIZE (16*1000*1000)

// debug prints
#define SME_DEBUG_LVL LVL_EXP

#endif /* __XCONFIG_H__ */
