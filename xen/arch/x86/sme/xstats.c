/*
 * xstats.c
 *
 * created on: Sep 27, 2017
 * author: aasthakm
 */

#include "xstats.h"
#include "xdebug.h"

#include <xen/stdarg.h>
#include <asm-x86/div64.h>

int
init_sme_stats (sme_stat *stat, char *name, uint64_t start, uint64_t end, uint64_t step)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int size;
#endif

  memcpy (stat->name, name, strlen (name));
  stat->start = start;
  stat->end = end;
  stat->step = step;

  stat->count = 0;
  stat->min = 99999999999;
  stat->max = 0;
  stat->min_idx = 0;
  stat->max_idx = 0;

#if SME_CONFIG_STAT_DETAIL == 1
  size = (end - start) / step + 1;
  stat->dist = xzalloc_array (uint64_t, size);

  if (!stat->dist)
    return -ENOMEM;
#endif

  return 0;
}

int
init_sme_stats_arr(sme_stat *stat, uint64_t start, uint64_t end, uint64_t step,
    char name[], ...)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int size;
#endif
  va_list args;

  va_start(args, name);
  vsnprintf(stat->name, sizeof(stat->name), name, args);
  va_end(args);
  stat->start = start;
  stat->end = end;
  stat->step = step;

  stat->count = 0;
  stat->min = 99999999999;
  stat->max = 0;
  stat->min_idx = 0;
  stat->max_idx = 0;

#if SME_CONFIG_STAT_DETAIL == 1
  size = (end - start) / step + 1;
  stat->dist = xzalloc_array (uint64_t, size);

  if (!stat->dist)
    return -ENOMEM;
#endif

  return 0;
}

void
add_point (sme_stat *stat, uint64_t value)
{
  int index;

  if (stat->step == 0)
    return;

  if (value >= stat->max) {
    stat->max = value;
    stat->max_idx = stat->count;
  }

  if (value < stat->min) {
    stat->min = value;
    stat->min_idx = stat->count;
  }

  index = (value - stat->start) / stat->step;

  if (value >= stat->end) {
    //append to the last bucket
    index = ((stat->end - stat->start) / stat->step) - 1;
  } else if (value < stat->start) {
    //append to the first bucket
    index = 0;
  }

#if SME_CONFIG_STAT_DETAIL == 1
  if (stat->dist)
    stat->dist[index]++;
#endif
  stat->count++;
  stat->sum += value;
}

void
add_custom_point (sme_stat *stat, int index, uint64_t value)
{
  int max_index = (stat->end - stat->start) / stat->step;

  if (value >= stat->max) {
    stat->max = value;
    stat->max_idx = stat->count;
  }

  if (value < stat->min) {
    stat->min = value;
    stat->min_idx = stat->count;
  }

  if (index < 0)
    index = stat->start;
  else if (index > max_index)
    index = max_index;

#if 0
  index = (value - stat->start) / stat->step;

  if (value >= stat->end) {
    //append to the last bucket
    index = ((stat->end - stat->start) / stat->step) - 1;
  } else if (value < stat->start) {
    //append to the first bucket
    index = 0;
  }
#endif

#if SME_CONFIG_STAT_DETAIL == 1
  if (stat->dist)
    stat->dist[index]++;
#endif
  stat->count++;
  stat->sum += value;
}

void
print_distribution (sme_stat *stat)
{

#if SME_CONFIG_STAT_DETAIL == 1
  int i;
  uint64_t bucket;
  int size = 0;
#endif

  //double avg = 0;
  uint64_t avg_div = 0;
  uint64_t avg_mod = 0;

  if (stat->count != 0) {
    avg_div = stat->sum;
    avg_mod = do_div(avg_div, stat->count);
    //avg_div = (stat->sum / (uint64_t) stat->count);
  }

  if (stat->count == 0 || stat->step == 0)
    return;

  xprintk2(LVL_EXP, "%s", "");
  xprintk2(LVL_EXP, "%s", "-----------------------------------");
  xprintk2(LVL_EXP, "%s", stat->name);

  xprintk2(LVL_EXP, "%s Total time: %lu", stat->name, stat->sum);

  if (stat->count > 0 && stat->dist) {
    xprintk2(LVL_EXP, "%s avg: %lu [%lu, %lu] count: %ld", stat->name, avg_div,
        stat->min, stat->max, stat->count);
    xprintk2(LVL_EXP, "%s min: %lu idx: %lu", stat->name, stat->min, stat->min_idx);
    xprintk2(LVL_EXP, "%s max: %lu idx: %lu", stat->name, stat->max, stat->max_idx);

#if SME_CONFIG_STAT_DETAIL == 1
    size = (stat->end - stat->start) / stat->step;
    for (i = 0; i < size; i++) {
      if (stat->dist[i] == 0)
        continue;

      bucket = stat->start + i * stat->step;
      xprintk2(LVL_EXP, "%s %-*lu\t%-lu", stat->name, 7, bucket, stat->dist[i]);
    }
#endif

  }
}

void
serialize_distribution (sme_stat *stat, char *ser, int len)
{
  char *pivot;
  int written = 0;
  int size;
#if SME_CONFIG_STAT_DETAIL == 1
  int i;
#endif

  if (stat == NULL) {
    ((int *) ser)[0] = (-1);
    return;
  }

  //reserve for index
  written += sizeof(int);

  pivot = ser + written;
  memcpy (pivot, stat, sizeof(sme_stat));
  written += sizeof(sme_stat);

  pivot = ser + written;
  size = (stat->end - stat->start) / stat->step;
#if SME_CONFIG_STAT_DETAIL == 1
  for (i = 0; i < size; i++) {
    if (written + sizeof(unsigned int) > len)
      break;

    ((unsigned int *) pivot)[i] = stat->dist[i];
  }

  ((int *) ser)[0] = i;
#endif
}

void
free_sme_stats (sme_stat *stat)
{
#if SME_CONFIG_STAT_DETAIL == 1
  xfree (stat->dist);
  stat->dist = NULL;
#endif
}

// === multi-variate stat ===
int
init_multi_stats_arr(multi_stat_t *stat, uint64_t start, uint64_t end,
    uint64_t step, char **spl_name, char common_name[], ...)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int size;
#endif
  int i;
  int tofree = 0;
  va_list args;
  char tmp_name[64];

  memset(tmp_name, 0, 64);
  va_start(args, common_name);
  vsnprintf(tmp_name, 64, common_name, args);
  va_end(args);

  stat->n_stat = 2;
  for (i = 0; i < stat->n_stat; i++) {
    snprintf(stat->name[i], 64, "%3s %s", spl_name[i], tmp_name);
#if SME_CONFIG_STAT_SCALE == SCALE_NS
    stat->start[i] = start;
    stat->end[i] = end;
    stat->step[i] = step;
    stat->min[i] = 99999999999;
#else
    stat->start[i] = ns_to_rdtsc(start);
    stat->end[i] = ns_to_rdtsc(end);
    stat->step[i] = ns_to_rdtsc(step);
    stat->min[i] = ns_to_rdtsc(99999999999);
#endif

    stat->count[i] = 0;
    stat->max[i] = 0;
    stat->min_idx[i] = 0;
    stat->max_idx[i] = 0;

#if SME_CONFIG_STAT_DETAIL == 1
    size = (end - start) / step + 1;
    stat->dist[i] = xzalloc_array (uint64_t, size);
    if (!stat->dist[i]) {
      tofree = 1;
      break;
    }
#endif
  }

  if (tofree == 1) {
#if SME_CONFIG_STAT_DETAIL == 1
    for (i = 0; i < stat->n_stat; i++) {
      if (stat->dist[i]) {
        xfree(stat->dist[i]);
      }
    }
#endif
    return -ENOMEM;
  }

  return 0;
}

void
add_multi_point(multi_stat_t *stat, uint64_t value[2])
{
  int index;
  int i;

  for (i = 0; i < stat->n_stat; i++) {
    if (value[i] >= stat->max[i]) {
      stat->max[i] = value[i];
      stat->max_idx[i] = stat->count[i];
    }

    if (value[i] < stat->min[i]) {
      stat->min[i] = value[i];
      stat->min_idx[i] = stat->count[i];
    }

    if (stat->step[i] == 0)
      continue;

    index = (value[i] - stat->start[i]) / stat->step[i];

    if (value[i] >= stat->end[i]) {
      //append to the last bucket
      index = ((stat->end[i] - stat->start[i]) / stat->step[i]) - 1;
    } else if (value[i] < stat->start[i]) {
      //append to the first bucket
      index = 0;
    }

#if SME_CONFIG_STAT_DETAIL == 1
    if (stat->dist[i])
      stat->dist[i][index]++;
#endif
    stat->count[i]++;
    stat->sum[i] += value[i];
  }
}

void
print_multi_distribution(multi_stat_t *stat)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int i;
  uint64_t bucket;
  int size = 0;
#endif
  int stat_i;

  uint64_t avg_div = 0;
  uint64_t avg_mod = 0;

  // TODO: change the semantics from return if even one zero
  // to return only if all zero
  for (stat_i = 0; stat_i < stat->n_stat; stat_i++) {
    if (stat->count[stat_i] == 0 || stat->step[stat_i] == 0)
      return;
  }

  xprintk2(LVL_EXP, "%s", "");
  xprintk2(LVL_EXP, "%s", "-----------------------------------");

  for (stat_i = 0; stat_i < stat->n_stat; stat_i++) {
    xprintk2(LVL_EXP, "%s", stat->name[stat_i]);
    if (stat->count[stat_i] != 0) {
      avg_div = stat->sum[stat_i];
      avg_mod = do_div(avg_div, stat->count[stat_i]);
      xprintk2(LVL_EXP, "%s avg: %lu [%lu, %lu] count: %ld", stat->name[stat_i]
          , rdtsc_to_ns(avg_div), rdtsc_to_ns(stat->min[stat_i])
          , rdtsc_to_ns(stat->max[stat_i]), stat->count[stat_i]);
      xprintk2(LVL_EXP, "%s min: %lu idx: %lu", stat->name[stat_i]
          , rdtsc_to_ns(stat->min[stat_i]), stat->min_idx[stat_i]);
      xprintk2(LVL_EXP, "%s max: %lu idx: %lu", stat->name[stat_i]
          , rdtsc_to_ns(stat->max[stat_i]), stat->max_idx[stat_i]);
    }

    if (stat->count[stat_i] > 0 && stat->dist[stat_i]) {

#if SME_CONFIG_STAT_DETAIL == 1
      size = (stat->end[stat_i] - stat->start[stat_i]) / stat->step[stat_i];
      for (i = 0; i < size; i++) {
        if (stat->dist[stat_i][i] == 0)
          continue;

        bucket = stat->start[stat_i] + i * stat->step[stat_i];
        xprintk2(LVL_EXP, "%s %-*lu\t%-lu", stat->name[stat_i], 7
            , rdtsc_to_ns(bucket), stat->dist[stat_i][i]);
      }
#endif

    }
  }
}

void
free_multi_stats(multi_stat_t *stat)
{
#if SME_CONFIG_STAT_DETAIL == 1
  int i;
  for (i = 0; i < stat->n_stat; i++) {
    xfree(stat->dist[i]);
    stat->dist[i] = NULL;
  }
#endif
}
