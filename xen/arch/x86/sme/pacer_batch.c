#include "xconfig.h"
#include "xdebug.h"
#include "xstats.h"
#include "xstats_decl.h"
#include "statq.h"
#include "mem_trans.h"
#include "pacer.h"
#include "pacer_obj.h"
#include "pacer_utils.h"
#include "sme_pkt.h"
#include "xprofq_hdr.h"
#include "xnicq_hdr.h"
#include "ptimer.h"
#include "tofree_q.h"

#include <asm/i387.h>

#if 0
extern union db_prod global_real_db;
extern union db_prod old_real_db;
extern u16 old_real_bd_prod;
extern u16 old_real_pkt_prod;
extern s_time_t global_db_write_time;
extern uint64_t event_ts;
#endif

#if 0
extern timerq_t pac_timerq;
extern active_timerq_t active_timerq;
extern pacer_obj_t pac_obj;
extern xtxdata_t guest_txdata;
extern xglobal_nicq_hdr_t guest_nicq;
extern xglobal_nicq_hdr_t guest_dummy_nicq;
extern xnic_freelist_t guest_nfl;
extern xnic_txintq_t guest_txintq;
extern ptimer_heap_t global_ptimer_heap;
extern int global_dummy_nicq_prod_idx;
extern tofreeq_t prof_tofreeq;
extern prof_update_q_t prof_updateq;
#endif
extern xprofq_hdr_t guest_profq_hdr;
extern xprofq_freelist2_t guest_profq_fl;
extern sme_htable_t pmap_htbl;

static inline int is_event_valid(s_time_t event_time, u64 event_tsc, s64 allowance)
{
#if SME_CONFIG_STAT_SCALE == SCALE_NS
  return ((NOW() - event_time) < allowance);
#else
  return ((s64) (rdtscp_ordered() - event_tsc) < allowance);
#endif
}

int
do_doorbell_combined(pacer_obj_t *pobj, s_time_t curr_epoch_start,
    u64 curr_epoch_tsc, int cond_spin)
{
  unsigned long flag = 0;
  u64 expire_time = curr_epoch_tsc;
  u64 last_cycle_count = 0;
  int ndb = (pobj->global_real_db.data.prod != pobj->old_real_db.data.prod);
  u64 db_delay = 0;

  SME_INIT_TIMER(tot_io_delay);
  SME_INIT_TIMER(tot_spin_delay);

  local_irq_save(flag);

  /*
   * hypace1: spin unconditionally
   * hypace2, 3: spin only if actual doorbell io
   */
  if (!cond_spin || ndb != 0) {
    SME_START_TIMER(tot_spin_delay);
    while ((last_cycle_count = rdtscp_ordered()) < expire_time) {
      //cpu_relax();
    }
    SME_POBJ_END_TIMER_ARR(tot_spin_delay, pobj->pbo_idx);
  }

  if (ndb) {
    SME_START_TIMER(tot_io_delay);

    writel((u32) pobj->global_real_db.raw, pobj->db_vaddr);
    barrier();
    smp_mb();

#if SME_CONFIG_STAT_SCALE == SCALE_NS
    db_delay = NOW() - curr_epoch_start;
#else
    db_delay = rdtsc_diff_to_ns(rdtscp_ordered(), curr_epoch_tsc);
#endif
    SME_POBJ_END_TIMER_ARR(tot_io_delay, pobj->pbo_idx);
  }

  pobj->old_real_db.raw = pobj->global_real_db.raw;
  pobj->old_real_bd_prod = *(pobj->guest_txdata.tx_bd_prod_p);
  pobj->old_real_pkt_prod = *(pobj->guest_txdata.tx_pkt_prod_p);

#if SME_CONFIG_STAT
  if (!cond_spin || ndb != 0) {
    SME_POBJ_ADD_COUNT_POINT_ARR(delay_after_spin,
        rdtsc_diff_to_ns(last_cycle_count, expire_time), pobj->pbo_idx);
  }
  if (ndb) {
    SME_POBJ_ADD_COUNT_POINT_ARR(relevant_delay_after_spin,
        rdtsc_diff_to_ns(last_cycle_count, expire_time), pobj->pbo_idx);
    SME_POBJ_ADD_COUNT_POINT_ARR(relevant_db_delay, db_delay, pobj->pbo_idx);
  }
#endif

  local_irq_restore(flag);

  return ndb;
}

/*
 * only supports SLOTSHARE=0, CONFIG_DUMMY=DUMMY_OOB,
 * and CONFIG_NIC_QUEUE=CONFIG_NQ_GLOBAL
 */
int
prep_merged_nicq_batch_io(pacer_obj_t *pobj, timer_obj_t *tobj,
    int *real_per_batch, int *dummy_per_batch)
{
  int prep_ret = -EINVAL;
  xtxdata_t *x_txdata = NULL;
  struct bnx2x_fp_txdata *gtxdata = NULL;
  u16 bd_prod;
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  nic_txdata_t *nic_data_elem = NULL;
  struct eth_tx_start_bd *start_bd = NULL;
  u64 data_dma_addr = 0;
  void *data_vaddr = NULL;
  void *tcph = NULL;
  uint32_t print_seqno = 0, print_seqack = 0;
  uint16_t ip_id = 0;
  uint16_t orig_bd_prod = 0;
  uint32_t old_prod, new_prod;
  int guest_prof_slot_idx = 0;
  int32_t push_timerq_idx = 0;
  int curr_idx = 0;
#if SME_DEBUG_LVL <= LVL_INFO
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
#endif
  uint32_t seqno = 0, seqack = 0;
  char *s_mac = NULL, *d_mac = NULL;
  uint32_t s_addr = 0, d_addr = 0;
  uint16_t s_port = 0, d_port = 0;
  int nbd = 0;
  int cons_idx = 0;
  int send_real = 0;
  int tx_avail = 0;

#if CONFIG_PACER_CRYPTO > CRYPTO_NONE
  int r;
  unsigned char *input = NULL;
  SME_INIT_TIMER(bufenc);
  SME_INIT_TIMER(cpyenc);
#endif

  SME_INIT_TIMER(pkt_prep_delay);
  SME_INIT_TIMER(nicq_rm_delay);
  SME_INIT_TIMER(prep_real_delay);
  SME_INIT_TIMER(prep_dummy_delay);

  SME_START_TIMER(pkt_prep_delay);
  SME_START_TIMER(nicq_rm_delay);

  xprof = &tobj->xen_profp;
  gprof = tobj->profq_addr;
  x_txdata = &pobj->guest_txdata;
  gtxdata = pobj->txdata;

  old_prod = pobj->global_real_db.data.prod;
  orig_bd_prod = *(x_txdata->tx_bd_prod_p);
  guest_prof_slot_idx = gprof->profq_idx;
  push_timerq_idx = tobj - &pobj->pac_timerq.queue[0];
  curr_idx = tobj->timer_idx;
#if SME_DEBUG_LVL <= LVL_INFO
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
#endif

  tx_avail = bnx2x_tx_avail(x_txdata);
  if (tx_avail < MAX_DESC_PER_TX_PKT)
    goto exit;

#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  send_real = (gprof->nic_cons_idx != gprof->nic_prod_idx);
#else
  cons_idx = nicq_remove_first(gprof->pcm_nicq_p, &pobj->guest_nicq, gprof);
  send_real = ((cons_idx >= 0 && cons_idx < pobj->guest_nicq.qsize) ? 1 : 0);
#endif

  SME_POBJ_END_TIMER_ARR(nicq_rm_delay, pobj->pbo_idx);

//  bd_prod = TX_BD(pobj->global_real_db.data.prod);
  bd_prod = TX_BD(gtxdata->tx_bd_prod);
  start_bd = &x_txdata->tx_desc_ring[bd_prod].start_bd;

  if (!x_txdata) {
    xprintk(LVL_EXP, "T(%d:%d) G%d PORT %u xtxdata %p"
        , (int) (tobj - &pobj->pac_timerq.queue[0]), tobj->timer_idx, gprof->profq_idx
        , xprof->conn_id.dst_port, x_txdata
        );
  }

  if (send_real) {

    prep_ret = 0;

    SME_START_TIMER(prep_real_delay);

    /*
     * nic_data_elem from the guest shared nicq
     */
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
    cons_idx = gprof->nic_cons_idx;
    nic_data_elem = tobj->nic_txdata_ptr_arr[gprof->nic_cons_idx];
    gprof->nic_cons_idx = (gprof->nic_cons_idx+1) % NIC_DATA_QSIZE;
#else
    nic_data_elem = pobj->guest_nicq.queue[cons_idx];
#endif

    /*
     * copy nic_data_elem fields into the next slot in the physical nic queue
     */
    nbd = prep_merged_nicq_slot(tobj, nic_data_elem, send_real, x_txdata,
        gtxdata, &pobj->guest_txintq);

    data_dma_addr = BD_UNMAP_ADDR(start_bd);
    data_vaddr = get_vaddr_from_guest_maddr(data_dma_addr);
    prefetchw(data_vaddr);

#if SME_DEBUG_LVL <= LVL_INFO
    if (!data_vaddr) {
      guest_prof_slot_idx = gprof->profq_idx;
      push_timerq_idx = tobj - &pobj->pac_timerq.queue[0];
      curr_idx = tobj->timer_idx;
      ipstr(xprof->conn_id.out_ip, x_out_ipstr);
      ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
      xprintk(LVL_EXP, "T(%d:%d) G%d [%s %u, %s %u] real %d\n"
          "NXT %d CWM %d P %d R %d D %d paused %d state %d "
          "DB %u BD %u %u PKT %u %u nicq cons_idx %d max %u\n"
          "mapping %0lx vaddr %p tx_avail %d"
          , push_timerq_idx, curr_idx, guest_prof_slot_idx
          , x_out_ipstr, xprof->conn_id.out_port
          , x_dst_ipstr, xprof->conn_id.dst_port
          , send_real, gprof->next_timer_idx, gprof->cwnd_watermark
          , gprof->num_timers, atomic_read(&gprof->real_counter)
          , gprof->dummy_counter, atomic_read(&gprof->is_paused), gprof->state
          , pobj->global_real_db.data.prod
          , *(x_txdata->tx_bd_prod_p), *(x_txdata->tx_bd_cons_p)
          , *(x_txdata->tx_pkt_prod_p), *(x_txdata->tx_pkt_cons_p)
          , cons_idx, gprof->max_seqno, data_dma_addr, data_vaddr, tx_avail
          );
    }
#endif

    // XXX: check if we really need max_seqno with guest
    tcph = data_vaddr + 34;
    seqno = __cpu_to_be32(*(uint32_t *) (tcph + sizeof(uint32_t)));
    seqack = __cpu_to_be32(*(uint32_t *) (tcph + 2*sizeof(uint32_t)));

    ip_id = __cpu_to_be16(*(uint32_t *) ((data_vaddr + 14) + sizeof(uint32_t)));
    print_seqno = seqno;
    print_seqack = seqack;
#if SME_DEBUG_LVL <= LVL_INFO
#endif

    SME_POBJ_END_TIMER_ARR(prep_real_delay, pobj->pbo_idx);
  } else { /* DUMMY */

    /*
     * XXX: do not reorder the sequence of operations in this branch!!!
     * in principle it is possible to set hdrs before setting the packet
     * information in the physical nic queue. but for uniformity of API
     * usage, this order is chosen. and it works. so no need to change.
     */
    prep_ret = 1;

    SME_START_TIMER(prep_dummy_delay);

    /*
     * values to set in the dummy's packet headers
     */
    s_addr = xprof->conn_id.out_ip;
    d_addr = xprof->conn_id.dst_ip;
    s_port = xprof->conn_id.out_port;
    d_port = xprof->conn_id.dst_port;
    s_mac = gprof->src_mac_p;
    d_mac = gprof->dst_mac_p;
    seqack = *(gprof->last_real_ack_p);

    /*
     * nic_data_elem contains metadata fields for the next dummy buffer
     * to use for transmission.
     */
    nic_data_elem = pobj->guest_dummy_nicq.queue[pobj->global_dummy_nicq_prod_idx];

    /*
     * first update the mac addresses in nic_data_elem, since they
     * are used to set mac address in the slot of the physical nic queue
     */
    ether_addr_copy((u8 *) nic_data_elem->s_mac, (u8 *) s_mac);
    ether_addr_copy((u8 *) nic_data_elem->d_mac, (u8 *) d_mac);

    pobj->global_dummy_nicq_prod_idx =
      (pobj->global_dummy_nicq_prod_idx+1) % pobj->guest_dummy_nicq.qsize;

    /*
     * copy nic_data_elem fields into the next slot in the physical nic queue
     */
    nbd = prep_merged_nicq_slot(tobj, nic_data_elem, send_real, x_txdata,
        gtxdata, &pobj->guest_txintq);

    /*
     * dma addr in the nic queue slot was just populated in prep_merged_nicq_slot
     * but bd_prod would have incremented in that function, but we cached the
     * start_bd pointer at the beginning of the function itself, before the
     * increment above. get the vaddr of the payload buffer from the dma addr.
     */
    data_dma_addr = BD_UNMAP_ADDR(start_bd);
    data_vaddr = get_vaddr_from_guest_maddr(data_dma_addr);
    prefetchw(data_vaddr);

    /*
     * commit to a seq# for dummy packet, and update tcp's shared variables.
     * this should be done as close to the actual transmission of the dummy,
     * since it also updates shared_dummy_out count in tcp sock, which the guest
     * may sync at some point to count this dummy packet for flow and cwnd
     * adjustment.
     */
    update_shared_pointers(gprof, &seqno);

    print_seqno = seqno;
    print_seqack = htonl(seqack);
#if SME_DEBUG_LVL <= LVL_INFO
    if (!data_vaddr) {
      guest_prof_slot_idx = gprof->profq_idx;
      push_timerq_idx = tobj - &pobj->pac_timerq.queue[0];
      curr_idx = tobj->timer_idx;
      ipstr(xprof->conn_id.out_ip, x_out_ipstr);
      ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
      xprintk(LVL_EXP, "T(%d:%d) G%d [%s %u, %s %u] real %d\n"
          "NXT %d CWM %d P %d R %d D %d paused %d state %d"
          "DB %u BD %u %u PKT %u %u nicq cons_idx %d seq %u:%u seqack %u max %u\n"
          "mapping %0lx vaddr %p tx_avail %d"
          , push_timerq_idx, curr_idx, guest_prof_slot_idx
          , x_out_ipstr, xprof->conn_id.out_port
          , x_dst_ipstr, xprof->conn_id.dst_port
          , send_real, gprof->next_timer_idx, gprof->cwnd_watermark
          , gprof->num_timers, atomic_read(&gprof->real_counter)
          , gprof->dummy_counter, atomic_read(&gprof->is_paused), gprof->state
          , pobj->global_real_db.data.prod
          , *(x_txdata->tx_bd_prod_p), *(x_txdata->tx_bd_cons_p)
          , *(x_txdata->tx_pkt_prod_p), *(x_txdata->tx_pkt_cons_p)
          , cons_idx, seqno, seqack, print_seqack, gprof->max_seqno
          , data_dma_addr, data_vaddr, tx_avail
          );
    }

    set_payload_in_dummy(data_vaddr + DUMMY_PAYLOAD_OFF,
        (uint16_t) guest_prof_slot_idx,
        (uint16_t) atomic_read(&gprof->real_counter),
        (uint16_t) gprof->dummy_counter, (uint16_t) curr_idx,
        (uint16_t) gprof->num_timers, (uint32_t) gprof->cwnd_watermark,
        *(x_txdata->tx_bd_prod_p), *(x_txdata->tx_bd_cons_p),
        *(x_txdata->tx_pkt_prod_p), *(x_txdata->tx_pkt_cons_p));
#endif

    /*
     * populate the packet headers in the payload buffer. as you can see,
     * this can be done even after the address of the payload is set in the
     * physical nic queue.
     *
     * in this case, we are getting the seq# from tcp_sock, so need to htonl
     */
    set_hdr_in_dummy(data_vaddr, s_mac, d_mac, s_addr, d_addr, s_port, d_port,
        htonl(seqno), print_seqack, 0, gprof->last_skb_snd_window);

    SME_POBJ_END_TIMER_ARR(prep_dummy_delay, pobj->pbo_idx);
  }

  // update max_seqno in shared profile before sending pkt on wire
  // incoming ack is guaranteed to arrive after max_seqno updated in prof
  if (seqno > gprof->max_seqno)
    gprof->max_seqno = seqno;

#if CONFIG_PACER_CRYPTO == CRYPTO_MBEDTLS

  SME_START_TIMER(cpyenc);

  // Encryption to be done for both reals and dummies
  input = ((unsigned char *) data_vaddr) + MANGLE_START_OFFSET;
  memset(pobj->input, 0, ENC_BUF_SIZE);
  memcpy(pobj->input, input, nic_data_elem->txbd_nbytes - MANGLE_START_OFFSET);

  SME_POBJ_END_TIMER_ARR(cpyenc, pobj->pbo_idx);

  pobj->nc_off = 0;
  pobj->nonce[3] = 0;

  SME_START_TIMER(bufenc);

  r = mbedtls_aes_crypt_ctr(&pobj->ctx,
      nic_data_elem->txbd_nbytes - MANGLE_START_OFFSET,
      &pobj->nc_off, (unsigned char *) pobj->nonce,
      pobj->stream_block, pobj->input, pobj->output);

  SME_POBJ_END_TIMER_ARR(bufenc, pobj->pbo_idx);
#if 0
  memcpy(((char*) data_vaddr) + MANGLE_START_OFFSET, output,
      nic_data_elem->txbd_nbytes - MANGLE_START_OFFSET);
#endif
#endif

#if CONFIG_PACER_CRYPTO == CRYPTO_OPENSSL
  SME_START_TIMER(cpyenc);

  // Encryption to be done for both reals and dummies
  input = ((unsigned char *) data_vaddr) + MANGLE_START_OFFSET;
  memset(pobj->input, 0, ENC_BUF_SIZE);
  memcpy(pobj->input, input, nic_data_elem->txbd_nbytes - MANGLE_START_OFFSET);

#if 0
  free_aes(&pobj->e_ctx, &pobj->d_ctx);
  init_aes(&pobj->e_ctx, &pobj->d_ctx, &pobj->e_state, &pobj->d_state,
      pobj->key, pobj->iv);
#else
  reset_aes(&pobj->e_ctx, &pobj->d_ctx);
#endif

  SME_POBJ_END_TIMER_ARR(cpyenc, pobj->pbo_idx);

  SME_START_TIMER(bufenc);

  r = do_openssl_enc(pobj->e_ctx, &pobj->e_state, pobj->input,
      nic_data_elem->txbd_nbytes - MANGLE_START_OFFSET, pobj->output);

  SME_POBJ_END_TIMER_ARR(bufenc, pobj->pbo_idx);
#endif

  pobj->global_real_db.data.prod += nbd;

  if (send_real) {
    xprofile_update_packet_counter(xprof, 1, PKT_REAL);
    xprofile_update_gprof_packet_counter(xprof, gprof, 0, PKT_REAL, 1);

    // must be checked after incrementing real_counter
    if (atomic_read(&gprof->real_counter) == gprof->tail)
      xprof->last_real_idx = gprof->next_timer_idx;

#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
    free_nicq_slot(&pobj->guest_nfl, cons_idx);
#endif
//    (*real_per_batch) += 1;
  } else {
    // must be checked before incrementing dummy_counter
    if (gprof->dummy_counter == 0 && gprof->next_timer_idx != 0)
      xprof->first_dummy_idx = gprof->next_timer_idx;

    xprofile_update_packet_counter(xprof, 1, PKT_DUMMY);
    xprofile_update_gprof_packet_counter(xprof, gprof, 0, PKT_DUMMY, 1);
    /*
     * REMOVE COMMENT
     * XXX: ridiculous behaviour since moving all global datastructures
     * into pacer_obj_t. dummy_per_batch, which is passed by reference
     * from the caller and its caller becomes null here, without it being
     * touched. there is no memory corruption above, since the code in
     * this function has been unmodified otherwise, and has been working
     * for a long time. also, simply enabling debug prints at the top of
     * the function makes the null pointer problem go away, and
     * dummy_per_batch pointer has the same value as in the caller.
     * assigning *dummy_per_batch to another local variable, incrementing
     * the local variable, and then assigning back to *dummy_per_batch
     * did not help. and it does not happen with real_per_batch above.
     * could this be an issue with generating assembly from the code?
     */
//    *dummy_per_batch = *dummy_per_batch + 1;
  }

  SME_POBJ_END_TIMER_ARR(pkt_prep_delay, pobj->pbo_idx);

exit:

#if 0
  {
    pkts_elem_t e;
    e.ts = rdtsc_ordered();
    e.port = xprof->conn_id.dst_port;
    e.seqno = print_seqno;
    e.seqack = print_seqack;
    e.ip_id = ip_id;
    e.db = pobj->global_real_db.data.prod;
    e.pkt_prod = *(x_txdata->tx_pkt_prod_p);
    e.pkt_cons = *(x_txdata->tx_pkt_cons_p);
    e.pkt_type = send_real;
    put_generic_q(&pobj->pkts_q, (void *) &e);
  }
#endif

  new_prod = pobj->global_real_db.data.prod;

#if SME_DEBUG_LVL <= LVL_INFO
  xprintk(LVL_EXP, "T(%d:%d) G%d PORT %u, %u expired %ld REAL %d DB %u nbd %d\n"
      "NXT %d CWM %d P %d R %d D %d paused %d state %d HO %d HS %d "
      "NIC DB %u => %u BD %u %u PKT %u %u seq %u:%u seqack hn %u %u nh %u"
      "\nmapping %0lx vaddr %p start_bd %p tx_avail %d last_ack_p %0lx %p"
      "\nhn.hn %u hn.nh %u nh.nh %u nh.hn %u max %u"
      , (int) (tobj - &pobj->pac_timerq.queue[0]), tobj->timer_idx, gprof->profq_idx
      , xprof->conn_id.out_port, xprof->conn_id.dst_port
      , tobj->true_expires, send_real
      , pobj->global_real_db.data.prod, nbd
      , gprof->next_timer_idx, gprof->cwnd_watermark
      , gprof->num_timers, atomic_read(&gprof->real_counter), gprof->dummy_counter
      , atomic_read(&gprof->is_paused), gprof->state
      , tobj->ptimer.heap_offset, GET_PHEAP_SIZE(&pobj->global_ptimer_heap)
      , old_prod, new_prod, orig_bd_prod
      , *x_txdata->tx_bd_cons_p, *x_txdata->tx_pkt_prod_p, *x_txdata->tx_pkt_cons_p
      , print_seqno, seqack, htonl(seqack), print_seqack, ntohl(seqack)
      , data_dma_addr, data_vaddr, start_bd, tx_avail
      , gprof->last_real_ack_maddr, gprof->last_real_ack_p
      , htonl(htonl(seqack)), htonl(ntohl(seqack)), ntohl(ntohl(seqack))
      , ntohl(htonl(seqack)), gprof->max_seqno
      );
#endif

  return prep_ret;
}

void
free_profile(pacer_obj_t *pobj, timer_obj_t *tobj)
{
  int timer_obj_idx = 0;
  profq_elem_t *gprof = tobj->profq_addr;
  SME_INIT_TIMER(heap_remove_delay);

#if SME_DEBUG_LVL <= LVL_INFO
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  uint16_t x_out_port = 0, x_dst_port = 0;
  xprofile_t *xprof = &tobj->xen_profp;
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  x_out_port = xprof->conn_id.out_port;
  x_dst_port = xprof->conn_id.dst_port;
#endif

  timer_obj_idx = tobj - &pobj->pac_timerq.queue[0];

  SME_START_TIMER(heap_remove_delay);

  remove_from_ptimer_heap(&pobj->global_ptimer_heap, &tobj->ptimer);

  SME_POBJ_END_TIMER_ARR(heap_remove_delay, pobj->pbo_idx);

#if SME_DEBUG_LVL <= LVL_INFO
  xprintk(LVL_INFO, "FREE T%d G%d [%s %u, %s %u] state %d paused %d "
      "NXT %d CWM %d P %d R %d D %d max_seqno %u"
      , timer_obj_idx, gprof->profq_idx
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , gprof->state, atomic_read(&gprof->is_paused)
      , gprof->next_timer_idx, gprof->cwnd_watermark, gprof->num_timers
      , atomic_read(&gprof->real_counter), gprof->dummy_counter, gprof->max_seqno
      );
#endif

  if (gprof->dummy_counter > 0) {
    SME_POBJ_ADD_COUNT_POINT_ARR(free_dummy_cnt, gprof->dummy_counter, pobj->pbo_idx);
  }
  SME_POBJ_ADD_COUNT_POINT_ARR(free_real_cnt, atomic_read(&gprof->real_counter),
      pobj->pbo_idx);

  SME_POBJ_ADD_COUNT_POINT_ARR(first_dummy_idx, tobj->xen_profp.first_dummy_idx,
      pobj->pbo_idx);
  SME_POBJ_ADD_COUNT_POINT_ARR(last_real_idx, tobj->xen_profp.last_real_idx,
      pobj->pbo_idx);

  tobj->true_expires = -1;
  tobj->timer_idx = -1;
  tobj->type = 0;

  cleanup_xprofile_unlocked(&tobj->xen_profp);
  gprof->state = PSTATE_XEN_COMPL;
  atomic_set(&gprof->is_paused, 0);
  unmap_shared_pointers(gprof);

  push_one_timer(&pobj->pac_timerq, timer_obj_idx);
  put_one_profq_freelist2(&pobj->guest_profq_fl, tobj->profq_addr->profq_idx);
}

int
free_profiles(pacer_obj_t *pobj, s_time_t curr_epoch_start, u64 curr_epoch_tsc,
    int *other_count, int *other_overflow)
{
  int ret = 0;
  tofreeq_elem_t tofree_elem;
  profq_elem_t *gprof = NULL;
  unsigned long atq_flag = 0;
#if 0
  int prev_idx = 0;
  int next_idx = 0;
  int found = 0;
  timer_obj_t *prev_tobj = NULL;
#endif
  timer_obj_t *lookup_tobj = NULL;
  int free_cnt = 0;
  s_time_t free_start = NOW();

  SME_INIT_TIMER(free_prof_delay);

  while (true) {
    // limit maximum time spent in freeing profiles (unsued)
    if (MAX_PROF_FREE_TIME > 0 && NOW() - free_start >= MAX_PROF_FREE_TIME)
      break;

    // epoch expired
    if ((MAX_OTHER_PER_EPOCH > 0 && *other_count >= MAX_OTHER_PER_EPOCH)
        || !is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)) {
      *other_overflow = *other_count;
      break;
    }

#if CONFIG_TIMERQ_SPINLOCK
    spin_lock_irqsave(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
    local_irq_save(atq_flag);
#endif

    ret = get_tofreeq(&pobj->prof_tofreeq, &tofree_elem);
    if (ret < 0) {
#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock_irqrestore(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
      local_irq_restore(atq_flag);
#endif
      return free_cnt;
    }

    SME_START_TIMER(free_prof_delay);

    gprof = pobj->guest_profq_hdr.queue[tofree_elem.profq_idx];
#if 1
    lookup_tobj = &pobj->pac_timerq.queue[gprof->tobj_idx];
#else
    found = 0;
    prev_idx = -1;

    next_idx = atomic_read(&pobj->active_timerq.first);
    while (next_idx >= 0) {
      lookup_tobj = &pobj->active_timerq.queue[next_idx];
      if (lookup_tobj->profq_addr == gprof) {
        found = 1;
        break;
      }

      prev_idx = next_idx;
      next_idx = atomic_read(&lookup_tobj->next);
    }

    if (!found) {
#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock_irqrestore(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
      local_irq_restore(atq_flag);
#endif
      continue;
    }

    if (prev_idx == -1) {
      atomic_set(&pobj->active_timerq.first, atomic_read(&lookup_tobj->next));
      atomic_set(&lookup_tobj->next, -1);
    } else {
      prev_tobj = &pobj->active_timerq.queue[prev_idx];
      atomic_set(&prev_tobj->next, atomic_read(&lookup_tobj->next));
    }
#endif

    free_profile(pobj, lookup_tobj);

    *other_count = *other_count + 1;
    free_cnt++;

#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock_irqrestore(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
      local_irq_restore(atq_flag);
#endif

    SME_POBJ_END_TIMER_ARR(free_prof_delay, pobj->pbo_idx);
  }

//  SME_POBJ_ADD_COUNT_POINT(free_prof_count, free_cnt);

  return free_cnt;
}

void
update_profiles(pacer_obj_t *pobj, s_time_t curr_epoch_start, u64 curr_epoch_tsc,
    int *ev1, int *ev2, int *ev3, int *ev4)
{
  int ret = 0;
  int ev1_count = 0, ev2_count = 0, ev3_count = 0, ev4_count = 0;
  prof_update_arg_t elem;
  profq_elem_t *gprof = NULL;
  SME_INIT_TIMER(prof_update1_delay);
  SME_INIT_TIMER(prof_update2_delay);
  SME_INIT_TIMER(cwnd_update3_delay);
  SME_INIT_TIMER(cwnd_update4_delay);

  while (true) {
    // epoch expired
    if (!is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE))
    //if (!((NOW() - curr_epoch_start) < (EPOCH_SIZE - PACER_SPIN_THRESHOLD)))
      break;

    ret = get_prof_update_q(&pobj->prof_updateq, &elem);
    if (ret < 0)
      return;

    gprof = pobj->guest_profq_hdr.queue[elem.profq_idx];

    if (gprof == NULL) {
      xprintk(LVL_EXP, "UPDATE Q prod %d cons %d cmd %d G%u gprof %p"
          , pobj->prof_updateq.prod_idx, pobj->prof_updateq.cons_idx
          , elem.cmd_type, elem.profq_idx, gprof
          );
    }

    switch (elem.cmd_type) {
      case P_UPDATE_PROF_ID:
        ev1_count++;
        SME_START_TIMER(prof_update1_delay);

        active_timerq_update_profile(pobj, &pobj->active_timerq, &pobj->pac_timerq,
            gprof, &pmap_htbl, elem.cmd_type, curr_epoch_start,
            &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(prof_update1_delay, pobj->pbo_idx);
        break;
      case P_EXTEND_PROF_RETRANS:
        ev2_count++;
        SME_START_TIMER(prof_update2_delay);

        active_timerq_update_profile(pobj, &pobj->active_timerq, &pobj->pac_timerq,
            gprof, &pmap_htbl, elem.cmd_type, curr_epoch_start,
            &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(prof_update2_delay, pobj->pbo_idx);
        break;

      case P_UPDATE_CWND_ACK:
        ev3_count++;
        SME_START_TIMER(cwnd_update3_delay);

        active_timerq_update_cwnd(pobj, &pobj->active_timerq, &pobj->pac_timerq,
            gprof, elem.cmd_type, curr_epoch_start, &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(cwnd_update3_delay, pobj->pbo_idx);
        break;
      case P_UPDATE_CWND_LOSS:
        ev4_count++;
        SME_START_TIMER(cwnd_update4_delay);

        active_timerq_update_cwnd(pobj, &pobj->active_timerq, &pobj->pac_timerq,
            gprof, elem.cmd_type, curr_epoch_start, &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(cwnd_update4_delay, pobj->pbo_idx);
        break;
    }

  }

  *ev1 = ev1_count;
  *ev2 = ev2_count;
  *ev3 = ev3_count;
  *ev4 = ev4_count;
}

/*
 * REMOVE COMMENT
 * previously this function had void return type. but this causes
 * tobj->timer_idx to not be set upon advancing timer before the
 * timer is picked again for next event processing. although not 100%
 * sure, compiler is doing some fancy optimization of this function
 * given that it is at the tail of the while loop in the caller
 * prepare_packets. adding barrier() or smp_mb() at the end of the
 * function didn't help. enabling spinlocks didn't help either.
 * enabling a debug print helps, but we cannot keep debug prints
 * enabled at runtime. changing the return type and returning the
 * timer idx is required (returning 0 is not enough).
 * could this be an issue with generating assembly from the code?
 */
int
advance_timer(pacer_obj_t *pobj, timer_obj_t *tobj)
{
  int ret = 0;
  uint64_t next_ts = 0;
  SME_INIT_TIMER(heap_insert_delay);

  if (tobj->profq_addr->state == PSTATE_XEN_COMPL && tobj->timer_idx == -1)
    return -XP_COMPL;

  ret = xprofile_pick_next_ts_unlocked(&tobj->xen_profp, tobj->profq_addr, &next_ts);
  if (ret >= 0) {
    SME_START_TIMER(heap_insert_delay);

    timer_obj_set_timestamp(tobj, next_ts, ret, &pobj->global_ptimer_heap);

    SME_POBJ_END_TIMER_ARR(heap_insert_delay, pobj->pbo_idx);
  } else if (ret == -XP_COMPL) {
    /*
     * We don't yet know if timer is to be freed. But as per current #timers,
     * and next_timer_idx, it is to be marked complete. However, do not free a
     * timer, until it is both marked complete, and its timer_idx is marked -1.
     */
    tobj->profq_addr->state = PSTATE_XEN_COMPL;
  }

  xprintk(LVL_DBG, "G %d:%d N %d W %d P %d status %d reqts %lu ret %d"
      , tobj->profq_addr->profq_idx, tobj->timer_idx
      , tobj->profq_addr->next_timer_idx
      , tobj->profq_addr->cwnd_watermark, tobj->profq_addr->num_timers
      , tobj->xen_profp.pid_replace_status, tobj->xen_profp.req_stime, ret
      );

  return ret;
}

int
dequeue_profiles(pacer_obj_t *pobj, int n_dequeue_ops, s_time_t curr_epoch_start,
    u64 curr_epoch_tsc, int *other_count, int *other_overflow)
{
  int ret = 0;
  int earliest_idx = 0;
  int pop_timerq_idx = 0;
  uint64_t next_ts = 0;
  int dq_cnt = 0;
  unsigned long atq_flag = 0;
  s_time_t sched_delay = 0;
  profq_elem_t *gprof = NULL;
  timer_obj_t *new_tobj = NULL;
#if SME_DEBUG_LVL <= LVL_DBG
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
#endif
  s_time_t dequeue_start = NOW();
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  int nic_data_ptr_idx = 0;
#endif

  SME_INIT_TIMER(dq_one_prof);
//  SME_INIT_TIMER(dq_insert_active);

  while (true) {
    // limit maximum time spent in dequeueing profiles (unused)
    if (MAX_DEQUEUE_TIME > 0 && NOW() - dequeue_start >= MAX_DEQUEUE_TIME)
      break;

    // epoch expired
    if ((MAX_OTHER_PER_EPOCH > 0 && *other_count >= MAX_OTHER_PER_EPOCH)
        || !is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)) {
      *other_overflow = *other_count;
      break;
    }

    SME_START_TIMER(dq_one_prof);
    earliest_idx = remove_first(&pobj->guest_profq_hdr);
    SME_POBJ_END_TIMER_ARR(dq_one_prof, pobj->pbo_idx);

    if (!(earliest_idx >= 1 && earliest_idx <= pobj->guest_profq_hdr.qsize - 2))
      break;

    gprof = pobj->guest_profq_hdr.queue[earliest_idx];
    populate_shared_pointers(gprof);
    ret = pop_one_timer(&pobj->pac_timerq, &pop_timerq_idx);
    if (ret < 0)
      break;

    new_tobj = &pobj->pac_timerq.queue[pop_timerq_idx];
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
    nic_data_ptr_idx = (earliest_idx - 1) * NIC_DATA_QSIZE;
    timer_obj_set_profile(new_tobj, TT_PROF, gprof, &pmap_htbl,
        &pobj->nic_txdata_ptr_arr[nic_data_ptr_idx]);
#else
    timer_obj_set_profile(new_tobj, TT_PROF, gprof, &pmap_htbl, NULL);
#endif
    gprof->tobj_idx = pop_timerq_idx;
    ret = xprofile_pick_next_ts(&new_tobj->xen_profp, gprof, &next_ts);

#if SME_DEBUG_LVL <= LVL_DBG
    ipstr(new_tobj->xen_profp.conn_id.out_ip, x_out_ipstr);
    ipstr(new_tobj->xen_profp.conn_id.dst_ip, x_dst_ipstr);
    xprintk(LVL_DBG, "G%d [%s %u, %s %u] T%d ret %d next ts %ld"
        , earliest_idx, x_out_ipstr, new_tobj->xen_profp.conn_id.out_port
        , x_dst_ipstr, new_tobj->xen_profp.conn_id.dst_port
        , pop_timerq_idx, ret, next_ts);
#endif

    if (ret >= 0 || ret == -XP_PAUSE) {
//      SME_START_TIMER(dq_insert_active);
#if CONFIG_TIMERQ_SPINLOCK
      spin_lock_irqsave(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
      local_irq_save(atq_flag);
#endif

//      active_timerq_insert_idx_at_head(&pobj->active_timerq, pop_timerq_idx);

      if (ret >= 0) {
        timer_obj_set_timestamp(new_tobj, next_ts, ret, &pobj->global_ptimer_heap);
        sched_delay = NOW() - new_tobj->xen_profp.req_stime;
        SME_POBJ_ADD_COUNT_POINT_ARR(sched_install, sched_delay, pobj->pbo_idx);

#if SME_DEBUG_LVL <= LVL_DBG
        xprintk(LVL_DBG, "Picked T%d [%s %u, %s %u] #timers %d "
            "head %d => %d ret %d FL prod %d cons %d ph sz %d"
            , pop_timerq_idx
            , x_out_ipstr, new_tobj->xen_profp.conn_id.out_port
            , x_dst_ipstr, new_tobj->xen_profp.conn_id.dst_port
            , gprof->num_timers, earliest_idx
            , atomic_read(&(pobj->guest_profq_hdr.queue[
                atomic_read(&pobj->profq_hdr_vaddr->u.sorted2.head)]->next))
            , ret
            , atomic_read(pobj->guest_profq_fl.prod_idx_p)
            , atomic_read(pobj->guest_profq_fl.cons_idx_p)
            , GET_PHEAP_SIZE(&pobj->global_ptimer_heap)
            );
#endif
      }
#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock_irqrestore(&pobj->active_timerq.active_lock, atq_flag);
#elif CONFIG_SAVE_IRQ
      local_irq_restore(atq_flag);
#endif

//      SME_POBJ_END_TIMER(dq_insert_active);

      ret = 0;
    } else {
      reset_timer_obj(new_tobj);
      push_one_timer(&pobj->pac_timerq, pop_timerq_idx);
      put_one_profq_freelist2(&pobj->guest_profq_fl, earliest_idx);
    }

    *other_count = *other_count + 1;

    dq_cnt++;
    if (dq_cnt == n_dequeue_ops)
      break;
  }

//  SME_POBJ_ADD_COUNT_POINT(dq_prof_count, dq_cnt);

  return dq_cnt;
}

int
prepare_packets(pacer_obj_t *pobj, s_time_t curr_epoch_start, u64 curr_epoch_tsc,
    int *real_per_batch, int *dummy_per_batch, int *overflow)
{
  timer_obj_t *tobj = NULL;
  profq_elem_t *gprof = NULL;
  ptimer_t *pt = NULL;
  int count = 0;
  int process_pkt = 0;
  int ret = 0;
  uint64_t prev_ts = 0;
  unsigned long flag = 0;
  int prep_ret = 0;

  SME_INIT_TIMER(heap_remove_delay);

  // XXX: check if can remove this eventually
  local_irq_save(flag);

  while (true) {

#if CONFIG_TIMERQ_SPINLOCK
    spin_lock(&pobj->active_timerq.active_lock);
#endif

#if CONFIG_HEAP_SPINLOCK
    spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

    process_pkt = ((GET_PHEAP_SIZE(&pobj->global_ptimer_heap) != 0)
        && ((pt = pobj->global_ptimer_heap.arr[1]) != NULL
          && is_timeslot_expired(pt->expires, curr_epoch_start)));

    if (!process_pkt || (MAX_PKTS_PER_EPOCH > 0 && count >= MAX_PKTS_PER_EPOCH) ||
        !is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)) {
      *overflow = process_pkt;

#if CONFIG_HEAP_SPINLOCK
      spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock(&pobj->active_timerq.active_lock);
#endif

      break;
    }

    SME_START_TIMER(heap_remove_delay);

    // pick earliest timer
    remove_from_ptimer_heap(&pobj->global_ptimer_heap, pt);

    SME_POBJ_END_TIMER_ARR(heap_remove_delay, pobj->pbo_idx);

    // prepare packet of the profile to which timer belongs
    // XXX: think about atomicity of critical sections
    tobj = (timer_obj_t *) pt->parent_tobj;
    gprof = tobj->profq_addr;

#if CONFIG_HEAP_SPINLOCK
    spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
    spin_lock(&tobj->xen_profp.timers_lock);
#endif

    /*
     * check if profile can be replaced at the beginning of a burst.
     * checks if all previous bursts aligned. currently we only have two bursts,
     * and we only check at the beginning of the first burst. this means
     * we give the application lesser slack that it could probably have.
     */
    if (tobj->xen_profp.pid_replace_status == PIDR_UNTESTED
        || tobj->xen_profp.pid_replace_status == PIDR_UNCHANGED) {
      ret = replace_xprofile(&tobj->xen_profp, gprof, &pmap_htbl, 0,
          curr_epoch_start);
      xprintk(LVL_DBG, "G%d:%d N %d R %d D %d W %d P %d status %d "
        "reqts %lu epoch %ld ret %d"
        , gprof->profq_idx, tobj->timer_idx, gprof->next_timer_idx
        , atomic_read(&gprof->real_counter), gprof->dummy_counter
        , gprof->cwnd_watermark, gprof->num_timers
        , tobj->xen_profp.pid_replace_status
        , tobj->xen_profp.req_stime, curr_epoch_start, ret
        );
      if (ret < 0) {
        switch (ret) {
          case -XP_NOCHG: tobj->xen_profp.pid_replace_status = PIDR_UNCHANGED;
                          break;
          case -XP_REPLC: break; // nop
          case -XP_EXPRD:
          case -XP_INVAL:
          case -XP_NOENT:
          default:        tobj->xen_profp.pid_replace_status = PIDR_REJECTED;
                          break;
        }
        goto prep;
      }

      reset_xprofile_unlocked(&tobj->xen_profp, gprof, &pmap_htbl, PIDR_REPLACED);
      SME_POBJ_ADD_COUNT_POINT_ARR(prof_switch_count, 1, pobj->pbo_idx);
      ret = xprofile_get_prev_ts_unlocked(&tobj->xen_profp, gprof, &prev_ts);
      if (ret >= 0 && prev_ts > tobj->true_expires
          && prev_ts >= curr_epoch_start) {

#if CONFIG_HEAP_SPINLOCK
        spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

        timer_obj_set_timestamp(tobj, prev_ts, ret, &pobj->global_ptimer_heap);

#if CONFIG_HEAP_SPINLOCK
        spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
        spin_unlock(&tobj->xen_profp.timers_lock);
        spin_unlock(&pobj->active_timerq.active_lock);
#endif

        continue;
      }
    }

prep:

    // skip preparing a packet if LATE PAUSE
    if (gprof->next_timer_idx > gprof->cwnd_watermark
        && gprof->cwnd_watermark >= 0) {
      atomic_set(&gprof->is_paused, 1);
      tobj->xen_profp.last_pause_stime = NOW();
      gprof->next_timer_idx = gprof->cwnd_watermark;

      /*
       * we are pausing a profile here, so should not set the
       * timer back into the heap. the timer will be set back
       * into the heap when a subsequent ack resumes the profile.
       */

#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock(&tobj->xen_profp.timers_lock);
      spin_unlock(&pobj->active_timerq.active_lock);
#endif

      continue;
    }

    // add packet at next prod idx in real or dummy queue
    prep_ret = prep_merged_nicq_batch_io(pobj, tobj, real_per_batch,
        dummy_per_batch);

    if (prep_ret == 0 || prep_ret == 1) {

#if CONFIG_HEAP_SPINLOCK
      spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

      // add tobj with ptimer at next_timer_idx back into heap
      ret = advance_timer(pobj, tobj);

      if (pobj->nic_overflow_bd_start) {
#if HYPACE_CONFIG_STAT
        nicstall_elem_t e;
#endif
        pobj->nic_overflow_end = rdtsc_ordered();
#if HYPACE_CONFIG_STAT
        e.start_time = pobj->nic_overflow_start;
        e.end_time = pobj->nic_overflow_end;
        e.start_bd = pobj->nic_overflow_bd_start;
        e.start_pkt = pobj->nic_overflow_pkt_start;
        e.start_prep = pobj->nic_overflow_prep_start;
        put_generic_q(&pobj->nicstall_q, (void *) &e);
#endif
        SME_POBJ_ADD_COUNT_POINT_ARR(nic_overflow_delay,
            rdtsc_diff_to_ns(pobj->nic_overflow_end, pobj->nic_overflow_start),
            pobj->pbo_idx);
        pobj->nic_overflow_bd_start = 0;
        pobj->nic_overflow_pkt_start = 0;
      }

#if CONFIG_HEAP_SPINLOCK
      spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

      count++;
      if (prep_ret == 0)
        *real_per_batch += 1;
      else
        *dummy_per_batch += 1;

    } else if (ret == -EINVAL) {
      // set back current timer into the heap
#if CONFIG_HEAP_SPINLOCK
      spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

      timer_obj_set_timestamp(tobj, tobj->true_expires, tobj->timer_idx,
          &pobj->global_ptimer_heap);

      if (!pobj->nic_overflow_bd_start) {
        pobj->nic_overflow_start = rdtsc_ordered();
        pobj->nic_overflow_bd_start = *(pobj->guest_txdata.tx_bd_prod_p);
        pobj->nic_overflow_pkt_start = *(pobj->guest_txdata.tx_pkt_prod_p);
        pobj->nic_overflow_prep_start = count;
      }
#if CONFIG_HEAP_SPINLOCK
      spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

    }

    xprintk(LVL_DBG, "G%d:%d N %d R %d D %d W %d P %d status %d "
        "reqts %lu epoch %ld ret %d"
        , gprof->profq_idx, tobj->timer_idx, gprof->next_timer_idx
        , atomic_read(&gprof->real_counter), gprof->dummy_counter
        , gprof->cwnd_watermark, gprof->num_timers
        , tobj->xen_profp.pid_replace_status, tobj->xen_profp.req_stime
        , curr_epoch_start, prep_ret
        );
#if CONFIG_TIMERQ_SPINLOCK
    spin_unlock(&tobj->xen_profp.timers_lock);
    spin_unlock(&pobj->active_timerq.active_lock);
#endif

  }
  SME_POBJ_ADD_COUNT_POINT_ARR(io_count, count, pobj->pbo_idx);
//  SME_POBJ_ADD_COUNT_POINT_ARR(real_count, *real_per_batch, pobj->pbo_idx);
//  SME_POBJ_ADD_COUNT_POINT_ARR(dummy_count, *dummy_per_batch, pobj->pbo_idx);

  local_irq_restore(flag);

  return count;
}

int
prepare_packets2(pacer_obj_t *pobj, s_time_t curr_epoch_start, u64 curr_epoch_tsc,
    int *real_per_batch, int *dummy_per_batch)
{
  timer_obj_t *tobj = NULL;
  profq_elem_t *gprof = NULL;
  ptimer_t *pt = NULL;
  int count = 0;
  int ret = 0;
  uint64_t prev_ts = 0;
  unsigned long flag = 0;
  int prep_ret = 0;
  int expire_count = 0;
  int processed_count = 0;
  int MAX_EXPIRE_COUNT = 128;
  ptimer_t *to_process_pt_arr[128];
  int total_expire_count = 0;
  int total_processed_count = 0;

  SME_INIT_TIMER(heap_insert_delay);
  SME_INIT_TIMER(heap_remove_delay);

  // XXX: check if can remove this eventually
  local_irq_save(flag);

  while (true) {

#if CONFIG_TIMERQ_SPINLOCK
    spin_lock(&pobj->active_timerq.active_lock);
#endif

#if CONFIG_HEAP_SPINLOCK
    spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

    expire_count = 0;
    processed_count = 0;
    while (is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)
        && expire_count < MAX_EXPIRE_COUNT
        && GET_PHEAP_SIZE(&pobj->global_ptimer_heap) != 0
        && ((pt = pobj->global_ptimer_heap.arr[1]) != NULL
          && is_timeslot_expired(pt->expires, curr_epoch_start))) {
      SME_START_TIMER(heap_remove_delay);
      // pick earliest timer
      remove_from_ptimer_heap(&pobj->global_ptimer_heap, pt);
      SME_POBJ_END_TIMER_ARR(heap_remove_delay, pobj->pbo_idx);
      to_process_pt_arr[expire_count] = pt;
      expire_count++;
    }

    total_expire_count += expire_count;

    if (!expire_count ||
        !is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)) {
#if CONFIG_HEAP_SPINLOCK
      spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock(&pobj->active_timerq.active_lock);
#endif
      break;
    }

#if CONFIG_HEAP_SPINLOCK
    spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

    while (is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)
        && processed_count < expire_count) {

      // prepare packet of the profile to which timer belongs
      // XXX: think about atomicity of critical sections
      tobj = (timer_obj_t *) to_process_pt_arr[processed_count]->parent_tobj;
      gprof = tobj->profq_addr;

#if CONFIG_TIMERQ_SPINLOCK
      spin_lock(&tobj->xen_profp.timers_lock);
#endif

      /*
       * check if profile can be replaced at the beginning of a burst.
       * checks if all previous bursts aligned. currently we only have two bursts,
       * and we only check at the beginning of the first burst. this means
       * we give the application lesser slack that it could probably have.
       */
      if (tobj->timer_idx == 0) {
        // TODO: need to modify replace_xprofile code, esp. check_profile_prefix
        ret = replace_xprofile(&tobj->xen_profp, gprof, &pmap_htbl, 0, curr_epoch_start);
        if (ret < 0) {
          tobj->xen_profp.pid_replace_status = PIDR_UNCHANGED;
          goto prep;
        }

        reset_xprofile_unlocked(&tobj->xen_profp, gprof, &pmap_htbl, PIDR_REPLACED);
        ret = xprofile_get_prev_ts_unlocked(&tobj->xen_profp, gprof, &prev_ts);
        if (ret >= 0 && prev_ts > tobj->true_expires
            && prev_ts >= curr_epoch_start) {

#if CONFIG_HEAP_SPINLOCK
          spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

          SME_START_TIMER(heap_insert_delay);

          timer_obj_set_timestamp(tobj, prev_ts, ret, &pobj->global_ptimer_heap);

          SME_POBJ_END_TIMER_ARR(heap_insert_delay, pobj->pbo_idx);

#if CONFIG_HEAP_SPINLOCK
          spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
          spin_unlock(&tobj->xen_profp.timers_lock);
#endif

          // advance to next dequeued timer
          processed_count++;

          continue;
        }
      }

prep:

      // skip preparing a packet if LATE PAUSE
      if (gprof->next_timer_idx > gprof->cwnd_watermark
          && gprof->cwnd_watermark >= 0) {
        atomic_set(&gprof->is_paused, 1);
        tobj->xen_profp.last_pause_stime = NOW();
        gprof->next_timer_idx = gprof->cwnd_watermark;

        /*
         * we are pausing a profile here, so should not set the
         * timer back into the heap. the timer will be set back
         * into the heap when a subsequent ack resumes the profile.
         */
#if CONFIG_TIMERQ_SPINLOCK
        spin_unlock(&tobj->xen_profp.timers_lock);
#endif

        // advance to next dequeued timer
        processed_count++;

        continue;
      }

      // add packet at next prod idx in real or dummy queue
      prep_ret = prep_merged_nicq_batch_io(pobj, tobj, real_per_batch,
          dummy_per_batch);

      if (!prep_ret) {
#if CONFIG_HEAP_SPINLOCK
        spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

        // add tobj with ptimer at next_timer_idx back into heap
        advance_timer(pobj, tobj);

#if CONFIG_HEAP_SPINLOCK
        spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

        count++;
      } else {
        // set back current timer into the heap
#if CONFIG_HEAP_SPINLOCK
        spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif

        SME_START_TIMER(heap_insert_delay);

        timer_obj_set_timestamp(tobj, tobj->true_expires, tobj->timer_idx,
            &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(heap_insert_delay, pobj->pbo_idx);

#if CONFIG_HEAP_SPINLOCK
        spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif
      }

#if CONFIG_TIMERQ_SPINLOCK
      spin_unlock(&tobj->xen_profp.timers_lock);
#endif

      processed_count++;
    }

    total_processed_count += processed_count;

    // put back unprocessed timers into heap
    if (!is_event_valid(curr_epoch_start, curr_epoch_tsc, ALLOWANCE)) {
#if CONFIG_HEAP_SPINLOCK
      spin_lock(&pobj->global_ptimer_heap.heap_lock);
#endif
      while (processed_count < expire_count) {
        tobj = (timer_obj_t *) to_process_pt_arr[processed_count]->parent_tobj;

        SME_START_TIMER(heap_insert_delay);

        timer_obj_set_timestamp(tobj, tobj->true_expires, tobj->timer_idx,
            &pobj->global_ptimer_heap);

        SME_POBJ_END_TIMER_ARR(heap_insert_delay, pobj->pbo_idx);

        processed_count++;
      }
#if CONFIG_HEAP_SPINLOCK
      spin_unlock(&pobj->global_ptimer_heap.heap_lock);
#endif

#if CONFIG_TIMERQ_SPINLOCK
      // this loop will execute only once
      spin_unlock(&pobj->active_timerq.active_lock);
#endif

      break;
    }

#if CONFIG_TIMERQ_SPINLOCK
    // this loop will execute only once
    spin_unlock(&pobj->active_timerq.active_lock);
#endif

    // TODO: remove this break
    //break;
  }

  SME_POBJ_ADD_COUNT_POINT_ARR(pkt_overflow_count,
      (total_expire_count - total_processed_count), pobj->pbo_idx);
//  SME_POBJ_ADD_COUNT_POINT(io_count, count);
//  SME_POBJ_ADD_COUNT_POINT(real_count, *real_per_batch);
//  SME_POBJ_ADD_COUNT_POINT(dummy_count, *dummy_per_batch);

  local_irq_restore(flag);

  return count;
}

void
xen_pacer_batch(void *arg)
{
  int cond_spin = 1;
  pacer_obj_t *pobj = (pacer_obj_t *) arg;
  timer_obj_t *doorbell_timer = &pobj->doorbell_timer;
  s_time_t curr_epoch_start = doorbell_timer->true_expires;
  s_time_t timer_start = curr_epoch_start - PACER_SPIN_THRESHOLD;
  s_time_t anchor_start = (cond_spin ? timer_start : curr_epoch_start);
  u64 curr_epoch_tsc = stime2tsc(curr_epoch_start);
#if SME_CONFIG_STAT_SCALE == SCALE_RDTSC
  u64 timer_tsc = stime2tsc(timer_start);
  u64 anchor_tsc = stime2tsc(anchor_start);
  u64 wakeup_tsc;
  u64 db_tsc;
  u64 prep_tsc;
  u64 last_tsc;
#endif
  u64 wakeup_delay;
  u64 prep_delay;
  int n_free;
  int n_dequeue;
  int n_prep;
  int orig_n_prep;
  int n_other;
  int n_db;
  int n_pkt = -1;
  int real_per_batch = 0;
  int dummy_per_batch = 0;
  int overflow = 0;
  int ev1 = 0, ev2 = 0, ev3 = 0, ev4 = 0;
  int other_count = 0;
  int other_overflow = 0;
#if HYPACE_CONFIG_STAT
  spurious_elem_t e;
#endif

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  wakeup_delay = NOW() - timer_start;
#else
  wakeup_tsc = rdtscp_ordered();
#endif

  if (!pobj->fpu_set) {
    pobj->fpu_set = 1;
    vcpu_restore_fpu_lazy(get_current());
    if (get_current()->arch.pv_vcpu.ctrlreg[0] & X86_CR0_TS) {
      get_current()->arch.pv_vcpu.ctrlreg[0] &= ~X86_CR0_TS;
    }
  }
  n_db = do_doorbell_combined(pobj, curr_epoch_start, curr_epoch_tsc, cond_spin);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  db_delay = NOW() - anchor_start;
#else
  db_tsc = rdtscp_ordered();
#endif

  update_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &ev1, &ev2, &ev3, &ev4);

  n_prep = prepare_packets(pobj, curr_epoch_start, curr_epoch_tsc,
      &real_per_batch, &dummy_per_batch, &overflow);
//  n_prep = prepare_packets2(pobj, curr_epoch_start, curr_epoch_tsc,
//      &real_per_batch, &dummy_per_batch);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  prep_delay = NOW() - anchor_start;
#else
  prep_tsc = rdtscp_ordered();
#endif

  n_dequeue = dequeue_profiles(pobj, 0, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

  n_free = free_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

  prefetchw(pobj->db_vaddr);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  last_delay = NOW() - anchor_start;
#else
  last_tsc = rdtscp_ordered();
#endif

//  SME_POBJ_ADD_COUNT_POINT(wakeup_delay, rdtsc_to_ns(wakeup_delay));
//  SME_POBJ_ADD_COUNT_POINT(db_delay, rdtsc_to_ns(db_delay));
//  SME_POBJ_ADD_COUNT_POINT(tot_prep_delay, rdtsc_to_ns(prep_delay));

#if SME_CONFIG_STAT_SCALE == SCALE_RDTSC
  wakeup_delay = rdtsc_diff_to_ns(wakeup_tsc, timer_tsc);
#endif
  if (n_db) {
    SME_POBJ_ADD_COUNT_POINT_ARR(relevant_db_wakeup_delay, wakeup_delay,
        pobj->pbo_idx);
  }
  if (overflow) {
    SME_POBJ_ADD_COUNT_POINT_ARR(pkt_overflow_count, n_prep, pobj->pbo_idx);
  } else {
    SME_POBJ_ADD_COUNT_POINT_ARR(pkt_overflow_count, 0, pobj->pbo_idx);
  }

  if (other_overflow) {
    SME_POBJ_ADD_COUNT_POINT_ARR(other_overflow_count, other_count, pobj->pbo_idx);
  } else {
    SME_POBJ_ADD_COUNT_POINT_ARR(other_overflow_count, 0, pobj->pbo_idx);
  }

#if HYPACE_CONFIG_STAT
  n_other = n_free + n_dequeue;
  if (n_other >= N_OTHER)
    n_other = N_OTHER - 1;
  orig_n_prep = n_prep;
  if (n_prep >= N_PREP)
    n_prep = N_PREP - 1;

  if (n_db || n_prep || n_other) {
    uint64_t val_arr[2] = { prep_tsc - db_tsc, last_tsc - db_tsc };
    if (!real_per_batch && !dummy_per_batch)
      n_pkt = 0;
    else if (real_per_batch && !dummy_per_batch)
      n_pkt = 1;
    else if (!real_per_batch && dummy_per_batch)
      n_pkt = 2;
    else
      n_pkt = 3;
    add_multi_point(&pobj->prof_stat[n_db][n_pkt][n_prep][n_other], val_arr);
  }

  prep_delay = rdtsc_diff_to_ns(prep_tsc, db_tsc);
  if (/* profiling config, capture all overflows */
      (MAX_PKTS_PER_EPOCH == 0 && overflow)
      /* during enforcement, capture "real" overflows */
      || (MAX_PKTS_PER_EPOCH > 0 && overflow && n_prep < MAX_PKTS_PER_EPOCH)
      /* spikes observed with lower #pkt preps in the epoch */
      || (n_prep > 5 && prep_delay/n_prep > 4000)
      /* interrupt handler delayed even more than expected max delay */
      || (wakeup_delay > PACER_SPIN_THRESHOLD)
      /* study overheads of profiling adjustment ops */
      || ev1 || ev2 || ev3 || ev4) {
    e.epoch_id = (curr_epoch_start - pobj->global_db_write_time) / EPOCH_SIZE;
    e.wakeup_delay = wakeup_delay;
    if (n_db)
      e.db_delay = rdtsc_diff_to_ns(db_tsc, timer_tsc) - PACER_SPIN_THRESHOLD;
    else
      e.db_delay = rdtsc_diff_to_ns(db_tsc, timer_tsc);
    e.prep_delay = prep_delay;
    e.last_delay = rdtsc_diff_to_ns(last_tsc, db_tsc);
    e.n_prep = orig_n_prep;
    e.n_other = n_other;
    e.n_ev1 = ev1;
    e.n_ev2 = ev2;
    e.n_ev3 = ev3;
    e.n_ev4 = ev4;
    e.overflow = overflow;
    e.db = n_db;
    e.n_pkt = n_pkt;
    put_generic_q(&pobj->spurious_q, (void *) &e);
  }
#endif /* HYPACE_CFG_STAT */

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2, NOW() - anchor_start, pobj->pbo_idx);
#else
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2,
      rdtsc_diff_to_ns(rdtscp_ordered(), anchor_tsc), pobj->pbo_idx);
#endif

  timer_obj_set_timer(doorbell_timer, curr_epoch_start + EPOCH_SIZE, 0,
      PACER_SPIN_THRESHOLD);

  n_pkt = 0;
  n_other = 0;
  prep_delay = 0;
  orig_n_prep = 0;
}

void
xen_pacer_batch2(void *arg)
{
  pacer_obj_t *pobj = (pacer_obj_t *) arg;
  timer_obj_t *doorbell_timer = &pobj->doorbell_timer;
  s_time_t curr_epoch_start = doorbell_timer->true_expires;
  s_time_t db_deadline = curr_epoch_start + EPOCH_SIZE - PACER_SPIN_THRESHOLD;
  u64 curr_epoch_tsc = stime2tsc(curr_epoch_start);
  u64 db_deadline_tsc = stime2tsc(db_deadline);
  u64 wakeup_delay = 0;
  u64 db_delay = 0;
  u64 prep_delay = 0;
  u64 last_delay = 0;
  int n_free = 0;
  int n_dequeue = 0;
  int n_prep = 0;
  int n_other = 0;
  int n_db = 0;
  int real_per_batch = 0;
  int dummy_per_batch = 0;
  int overflow = 0;
  int ev1 = 0, ev2 = 0, ev3 = 0, ev4 = 0;
  int other_count = 0;
  int other_overflow = 0;

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  wakeup_delay = NOW() - curr_epoch_start;
#else
  wakeup_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  update_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &ev1, &ev2, &ev3, &ev4);

  n_prep = prepare_packets(pobj, curr_epoch_start, curr_epoch_tsc,
      &real_per_batch, &dummy_per_batch, &overflow);
//  n_prep = prepare_packets2(pobj, curr_epoch_start, curr_epoch_tsc,
//      &real_per_batch, &dummy_per_batch);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  prep_delay = NOW() - curr_epoch_start;
#else
  prep_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  n_dequeue = dequeue_profiles(pobj, 0, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

  n_free = free_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

  prefetchw(pobj->db_vaddr);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  last_delay = NOW() - curr_epoch_start;
#else
  last_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  n_db = do_doorbell_combined(pobj, db_deadline, db_deadline_tsc, 1);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  db_delay = NOW() - db_deadline;
#else
  db_delay = rdtsc_diff_to_ns(rdtsc_ordered(), db_deadline_tsc);
#endif

  timer_obj_set_timer(doorbell_timer, curr_epoch_start + EPOCH_SIZE, 0, 0);

  SME_POBJ_ADD_COUNT_POINT_ARR(wakeup_delay, wakeup_delay, pobj->pbo_idx);
  SME_POBJ_ADD_COUNT_POINT_ARR(tot_prep_delay, prep_delay, pobj->pbo_idx);
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay, db_delay, pobj->pbo_idx);
  if (n_db) {
    SME_POBJ_ADD_COUNT_POINT_ARR(relevant_db_delay, db_delay, pobj->pbo_idx);
  }

#if HYPACE_CONFIG_STAT
  n_other = n_free + n_dequeue;
  if (n_other >= N_OTHER)
    n_other = N_OTHER - 1;
  if (n_prep >= N_PREP)
    n_prep = N_PREP - 1;

//  if (n_db || n_prep || n_other)
//    add_point(&pobj->prof_stat[n_db][0][n_prep][n_other], last_delay);
#endif /* HYPACE_CFG_STAT */

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2, NOW() - curr_epoch_start, pobj->pbo_idx);
#else
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2,
      rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc), pobj->pbo_idx);
#endif

  n_other = 0;
}

void
xen_pacer_batch3(void *arg)
{
  pacer_obj_t *pobj = (pacer_obj_t *) arg;
  timer_obj_t *doorbell_timer = &pobj->doorbell_timer;
  int handler_count = 0;
  s_time_t curr_epoch_start = 0;
  s_time_t db_deadline = 0;
  s_time_t next_epoch_start = 0;
  u64 curr_epoch_tsc;
  u64 db_deadline_tsc;
  u64 wakeup_delay = 0;
  u64 db_delay = 0;
  u64 free_delay = 0;
  u64 dequeue_delay = 0;
  u64 prep_delay = 0;
  u64 last_delay = 0;
  u64 expire_time = 0, last_cycle_count = 0;
  int n_free = 0;
  int n_dequeue = 0;
  int n_prep = 0;
  int n_other = 0;
  int n_db = 0;
  int real_per_batch = 0;
  int dummy_per_batch = 0;
  int ev1 = 0, ev2 = 0, ev3 = 0, ev4 = 0;
  int other_count = 0;
  int other_overflow = 0;

restart:
  curr_epoch_start = doorbell_timer->true_expires;
  db_deadline = curr_epoch_start + EPOCH_SIZE - PACER_SPIN_THRESHOLD;
  next_epoch_start = curr_epoch_start + EPOCH_SIZE;
  curr_epoch_tsc = stime2tsc(curr_epoch_start);
  db_deadline_tsc = stime2tsc(db_deadline);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  wakeup_delay = NOW() - curr_epoch_start;
#else
  wakeup_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  update_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &ev1, &ev2, &ev3, &ev4);

//  n_prep = prepare_packets(pobj, curr_epoch_start, curr_epoch_tsc);
  n_prep = prepare_packets2(pobj, curr_epoch_start, curr_epoch_tsc,
      &real_per_batch, &dummy_per_batch);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  prep_delay = NOW() - curr_epoch_start;
#else
  prep_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  n_dequeue = dequeue_profiles(pobj, 0, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  dequeue_delay = NOW() - curr_epoch_start;
#else
  dequeue_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  n_free = free_profiles(pobj, curr_epoch_start, curr_epoch_tsc, &other_count,
      &other_overflow);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  free_delay = NOW() - curr_epoch_start;
#else
  free_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  prefetchw(pobj->db_vaddr);

  n_db = do_doorbell_combined(pobj, db_deadline, db_deadline_tsc, 1);

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  db_delay = NOW() - db_deadline;
#else
  db_delay = rdtsc_diff_to_ns(rdtsc_ordered(), db_deadline_tsc);
#endif

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  last_delay = NOW() - curr_epoch_start;
#else
  last_delay = rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc);
#endif

  SME_POBJ_ADD_COUNT_POINT_ARR(wakeup_delay, wakeup_delay, pobj->pbo_idx);
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay, db_delay, pobj->pbo_idx);
  SME_POBJ_ADD_COUNT_POINT_ARR(tot_prep_delay, prep_delay, pobj->pbo_idx);

#if HYPACE_CONFIG_STAT
  n_other = n_free + n_dequeue;
  if (n_other >= N_OTHER)
    n_other = N_OTHER - 1;
  if (n_prep >= N_PREP)
    n_prep = N_PREP - 1;

//  if (n_db || n_prep || n_other)
//    add_point(&pobj->prof_stat[n_db][0][n_prep][n_other], free_delay);
#endif /* HYPACE_CFG_STAT */

#if SME_CONFIG_STAT_SCALE == SCALE_NS
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2, NOW() - curr_epoch_start, pobj->pbo_idx);
#else
  SME_POBJ_ADD_COUNT_POINT_ARR(db_delay2,
      rdtsc_diff_to_ns(rdtsc_ordered(), curr_epoch_tsc), pobj->pbo_idx);
#endif

  if (!(n_db || n_prep || n_other)) {
    // no work done in this interrupt, set next timer
    timer_obj_set_timer(doorbell_timer, next_epoch_start, 0, 0);
  } else {
    handler_count += 1;
    if (handler_count >= MAX_HP_PER_IRQ) {
      timer_obj_set_timer(doorbell_timer, next_epoch_start, 0, 0);
    } else {
      expire_time = stime2tsc(next_epoch_start);
      while ((last_cycle_count = rdtsc_ordered()) < expire_time) {
        // nop
      }
      doorbell_timer->true_expires = next_epoch_start;
      goto restart;
    }
  }
}
