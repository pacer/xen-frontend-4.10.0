/*
 * xstats.h
 *
 *  created on: Sep 27, 2017
 *  author: aasthakm
 */

#ifndef __XSTATS_H__
#define __XSTATS_H__

#include "xconfig.h"
#include <xen/lib.h>

typedef struct sme_stat
{
  char name[64]; //variable name

  uint64_t start;
  uint64_t end;
  uint64_t step;

  uint64_t count;
  uint64_t sum;

  uint64_t min;
  uint64_t max;
  uint64_t min_idx;
  uint64_t max_idx;

  uint64_t* dist; //the distribution

} sme_stat;

//extern sme_stat ****prof_stat;
//extern sme_stat ***multid_stat;

int
init_sme_stats (sme_stat* stat, char* name, uint64_t start, uint64_t end, uint64_t step);

int
init_sme_stats_arr(sme_stat *stat, uint64_t start, uint64_t end, uint64_t step,
    char name[], ...);

// take a value in ns
void
add_point (sme_stat* stat, uint64_t value);

void
add_custom_point (sme_stat* stat, int index, uint64_t value);

void
print_distribution (sme_stat* stat);

void
serialize_distribution (sme_stat* stat, char* ser, int len);

void
free_sme_stats (sme_stat* stat);

// === multi-variate stat ===
typedef struct multi_stat
{
  char name[2][64]; //variable name

  int n_stat;
  uint64_t start[2];
  uint64_t end[2];
  uint64_t step[2];

  uint64_t count[2];
  uint64_t sum[2];

  uint64_t min[2];
  uint64_t max[2];
  uint64_t min_idx[2];
  uint64_t max_idx[2];

  uint64_t* dist[2]; //the distribution

} multi_stat_t;

extern multi_stat_t ****prof_stat;

int
init_multi_stats_arr(multi_stat_t *stat, uint64_t start, uint64_t end,
    uint64_t step, char **spl_name, char common_name[], ...);

// take a value in ns
void
add_multi_point (multi_stat_t *stat, uint64_t value[2]);

void
print_multi_distribution (multi_stat_t *stat);

void
free_multi_stats (multi_stat_t *stat);

#if SME_CONFIG_STAT == 1

#define SME_DECL_STAT_EXTERN(name)	extern sme_stat* stats_##name
#define SME_DECL_STAT(name)	sme_stat* stats_##name
#define SME_INIT_STAT(name, desc, sta, end, st) \
	stats_##name = xzalloc(sme_stat); \
	if(!stats_##name) { \
		return; \
	} \
	if(init_sme_stats(stats_##name, desc, sta, end, st))  \
		return

#define SME_DEST_STAT(name, toprint) \
  if(stats_##name != NULL) {  \
    if (toprint) {  \
          print_distribution(stats_##name); \
    } \
    free_sme_stats(stats_##name); \
    xfree(stats_##name);  \
  }

#define SME_PRINT_STAT(name)  \
  if(stats_##name != NULL) {  \
    print_distribution(stats_##name); \
  }

#define SME_INIT_TIMER(name)  \
	s_time_t start_##name; \
	s_time_t end_##name

#define SME_START_TIME(name) start_##name
#define SME_END_TIME(name) end_##name

//#define SME_START_TIMER(name) getnstimeofday(&start_##name)
#if SME_CONFIG_STAT_SCALE == SCALE_NS
#define SME_START_TIMER(name) start_##name = NOW()

#define SME_END_TIMER(name) \
	end_##name = NOW(); \
	SME_ADD_TIME_POINT(name)

#define SME_SPENT_TIME(name)  \
	(SME_END_TIME(name) - SME_START_TIME(name))

#else
#define SME_START_TIMER(name) start_##name = rdtscp_ordered()

#define SME_END_TIMER(name) \
	end_##name = rdtscp_ordered(); \
	SME_ADD_TIME_POINT(name)

#define SME_SPENT_TIME(name)  \
	((SME_END_TIME(name) - SME_START_TIME(name)) * 1000/TSC_TO_NS_SCALE)

#endif /* SME_CONFIG_STAT_SCALE */

#define SME_ADD_TIME_POINT(name)  \
	(add_point(stats_##name, SME_SPENT_TIME(name)))

#define SME_ADD_COUNT_POINT(name, val)  \
	(add_point(stats_##name, val))

#define SME_ADD_CUSTOM_POINT(name, index, val)  \
  (add_custom_point(stats_##name, index, val))

#define SME_DECL_STAT_ARR_EXTERN(name, N) extern sme_stat *stats_##name[N]
#define SME_DECL_STAT_ARR(name, N)  \
  sme_stat *stats_##name[N]
#define SME_INIT_STAT_ARR(name, sta, end, st, N, desc, arg...)  \
  do {  \
    int stati;  \
    for (stati = 0; stati < N; stati++) { \
      stats_##name[stati] = xzalloc(sme_stat);  \
      init_sme_stats_arr(stats_##name[stati], sta, end, st, desc, stati); \
    } \
  } while (0)

#define SME_DEST_STAT_ARR(name, toprint, N) \
  do {  \
    int stati;  \
    for (stati = 0; stati < N; stati++) { \
      if ((sme_stat *) stats_##name[stati] != NULL) {  \
        if (toprint) {  \
          print_distribution((sme_stat *) stats_##name[stati]);  \
        } \
        free_sme_stats((sme_stat *) stats_##name[stati]);  \
        xfree((sme_stat *) stats_##name[stati]); \
      } \
    } \
  } while (0)

#define SME_ADD_TIME_POINT_ARR(name, stati) \
  (add_point(stats_##name[stati], SME_SPENT_TIME(name)))

#define SME_PTR_STAT(name, pobj)  \
  sme_stat *stats_##name = pobj->stats_##name

#define SME_PTR_STAT_ARR(name, pobj)  \
  sme_stat **stats_##name = pobj->stats_##name

#define SME_POBJ_INIT_STAT_ARR(name, sta, end, st, N, pidx, desc, arg...)  \
  do {  \
    int stati;  \
    for (stati = 0; stati < N; stati++) { \
      stats_##name[stati] = xzalloc(sme_stat);  \
      init_sme_stats_arr(stats_##name[stati], sta, end, st, desc, pidx); \
    } \
  } while (0)

#define SME_POBJ_ADD_COUNT_POINT(name, val) \
  (add_point(pobj->stats_##name, val))

#define SME_POBJ_ADD_COUNT_POINT_ARR(name, val, stati)  \
  (add_point(pobj->stats_##name[0], val))

#define SME_POBJ_END_TIMER(name)  \
  end_##name = rdtscp_ordered();  \
  SME_POBJ_ADD_TIME_POINT(name)

#define SME_POBJ_END_TIMER_ARR(name, stati) \
  end_##name = rdtscp_ordered();  \
  SME_POBJ_ADD_TIME_POINT_ARR(name, stati)

#define SME_POBJ_ADD_TIME_POINT(name) \
  (add_point(pobj->stats_##name, SME_SPENT_TIME(name)))

#define SME_POBJ_ADD_TIME_POINT_ARR(name, stati) \
  (add_point(pobj->stats_##name[0], SME_SPENT_TIME(name)))

#if 0
#define SME_END_TIMER_ARR(name, stati)  \
  getnstimeofday(&end_##name);  \
  SME_ADD_TIME_POINT_ARR(name, stati)
#endif

#if SME_CONFIG_STAT_SCALE == SCALE_NS
#define SME_END_TIMER_ARR(name, stati)  \
  end_##name = NOW(); \
  SME_ADD_TIME_POINT_ARR(name, stati)

#else
#define SME_END_TIMER_ARR(name, stati)  \
  end_##name = rdtscp_ordered(); \
  SME_ADD_TIME_POINT_ARR(name, stati)

#endif

#define SME_ADD_COUNT_POINT_ARR(name, val, stati) \
  (add_point(stats_##name[stati], val))

#else /* SME_CONFIG_STAT == 0 */
#define SME_DECL_STAT_EXTERN(name)
#define SME_DECL_STAT(name)
#define SME_INIT_STAT(name, desc, sta, end, st)
#define SME_DEST_STAT(name, toprint)
#define SME_PRINT_STAT(name)
#define SME_INIT_TIMER(name)
#define SME_START_TIMER(name)
#define SME_START_TIME(name)
#define SME_END_TIME(name)
#define SME_SPENT_TIME(name)
#define SME_ADD_TIME_POINT(name)
#define SME_END_TIMER(name)
#define SME_ADD_COUNT_POINT(name, val)
#define SME_ADD_CUSTOM_POINT(name, index, val)

#define SME_DECL_STAT_ARR_EXTERN(name, N)
#define SME_DECL_STAT_ARR(name, N)
#define SME_INIT_STAT_ARR(name, sta, end, st, N, desc, arg...)
#define SME_DEST_STAT_ARR(name, toprint, N)
#define SME_ADD_TIME_POINT_ARR(name, stati)
#define SME_END_TIMER_ARR(name, stati)
#define SME_ADD_COUNT_POINT_ARR(name, val, stati)
#endif

static inline uint64_t rdtscp_ordered(void)
{
  uint32_t low, high;

  __asm__ __volatile__("rdtscp" : "=a" (low), "=d" (high));

  return ((uint64_t)high << 32) | low;
}

#if SME_CONFIG_STAT_SCALE == SCALE_NS
static inline u64 rdtsc_to_ns(u64 rval)
{
  return rval;
}

static inline u64 ns_to_rdtsc(u64 nsval)
{
  return nsval;
}
#else
static inline u64 rdtsc_to_ns(u64 rval)
{
  return (rval * 1000)/TSC_TO_NS_SCALE;
}

static inline u64 ns_to_rdtsc(u64 nsval)
{
  return (nsval * TSC_TO_NS_SCALE)/1000;
}
#endif

static inline u64 rdtsc_diff_to_ns(u64 val1, u64 val2)
{
  return ((val1 - val2) * 1000)/TSC_TO_NS_SCALE;
}

static inline u64 ns_diff_to_rdtsc(u64 ns1, u64 ns2)
{
  return ((ns1 - ns2) * TSC_TO_NS_SCALE)/1000;
}

#endif /* __XSTATS_H__ */
