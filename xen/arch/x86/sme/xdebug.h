/*
 * xdebug.h
 *
 * created on: Mar 25, 2018
 * author: aasthakm
 *
 * SME wrapper around printk
 */
#ifndef __XDEBUG_H__
#define __XDEBUG_H__

#include <xen/time.h>
#include <xen/lib.h>

#define LVL_DBG 0
#define LVL_INFO 1
#define LVL_ERR 2
#define LVL_EXP 3

#define xprintk(LVL, F, A...)  \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(XENLOG_ERR "%s:%d/%-d [%ld] " F "\n"  \
          , __func__, __LINE__, smp_processor_id(), NOW(), A); \
    } \
  } while (0)

#define xprintk2(LVL, F, A...)  \
  do {  \
    if (SME_DEBUG_LVL <= LVL) { \
      printk(XENLOG_ERR  F "\n"  \
          , A); \
    } \
  } while (0)

#define xtrace  \
  do {  \
    if (SME_DEBUG_LVL <= LVL_EXP) { \
      printk(XENLOG_ERR "%s:%d/%-d [%ld]\n" \
          , __func__, __LINE__, smp_processor_id(), NOW()); \
    } \
  } while (0)

// caller must pass hash buffer of known size,
// and zero it a priori
static inline void
prt_hash(char *source, int source_len, char *hash)
{
  int it = 0;
  for (it = 0; it < source_len; it++) {
    snprintf(hash + (it*2), 3, "%02X", *(unsigned char *) (source + it));
  }
}

static inline void
ipstr(uint32_t ip, char *str)
{
  int i;
  uint8_t ipint[4];

  for (i = 0; i < 4; i++)
    ipint[i] = ((uint8_t *) &ip)[i];

  memset(str, 0, 16);
  snprintf(str, 16, "%d.%d.%d.%d", ipint[0], ipint[1], ipint[2], ipint[3]);
}

#endif /* __XDEBUG_H__ */
