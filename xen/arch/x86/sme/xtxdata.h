#ifndef __XTXDATA_H__
#define __XTXDATA_H__

#include "sme_guest.h"

typedef struct xtxdata {
  struct sw_tx_bd **tx_buf_ring;
  union eth_tx_bd_types *tx_desc_ring;
  union db_prod *tx_db;
  u16 *tx_pkt_prod_p;
  u16 *tx_pkt_cons_p;
  u16 *tx_bd_prod_p;
  u16 *tx_bd_cons_p;
  int *tx_ring_size_p;
} xtxdata_t;

void init_x_txdata(xtxdata_t *xtxdata, struct bnx2x_fp_txdata *gtxdata,
    sme_pacer_arg_t *xarg, int hp_i, union db_prod *global_real_db,
    union db_prod *old_real_db, u16 *old_real_bd_prod_p, u16 *old_real_pkt_prod_p);
void cleanup_x_txdata(xtxdata_t *xtxdata, struct bnx2x_fp_txdata *gtxdata, int real);

/*
 * this function can only be used for dummy queue, since each dummy pkt
 * is exactly one bd in dummy queue. otherwise, if a pkt can span multiple bds
 * (which may be the case for payload pkts), then we must check avail bds
 * in the queue.
 */
static inline u16 bnx2x_tx_pkt_avail(xtxdata_t *xtxdata)
{
  s16 used;
  u16 prod;
  u16 cons;

  prod = *(xtxdata->tx_pkt_prod_p);
  cons = *(xtxdata->tx_pkt_cons_p);
  used = SUB_S16(prod, cons);

  return (s16) (NUM_TX_BD - used);
}

static inline u16 bnx2x_tx_avail(xtxdata_t *xtxdata)
{
  s16 used;
  u16 prod;
  u16 cons;

  prod = *(xtxdata->tx_bd_prod_p);
  cons = *(xtxdata->tx_bd_cons_p);
  used = SUB_S16(prod, cons);

  return (s16) (*xtxdata->tx_ring_size_p - used);
}

#endif /* __XTXDATA_H__ */
