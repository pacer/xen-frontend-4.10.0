#ifndef __XSTATS_DECL_H__
#define __XSTATS_DECL_H__

SME_DECL_STAT_EXTERN(spin_count);
SME_DECL_STAT_ARR_EXTERN(delay_after_spin, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(relevant_delay_after_spin, MAX_HP_ARGS);
SME_DECL_STAT_EXTERN(delayed);
SME_DECL_STAT_EXTERN(event_delay);
SME_DECL_STAT_ARR_EXTERN(wakeup_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(relevant_db_wakeup_delay, MAX_HP_ARGS);

SME_DECL_STAT_EXTERN(active_lock_delay);
SME_DECL_STAT_EXTERN(timer_lock_delay);
SME_DECL_STAT_ARR_EXTERN(heap_insert_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(heap_remove_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(dq_one_prof, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(dq_insert_active, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(pkt_prep_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(nicq_rm_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(prep_real_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(prep_dummy_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(dq_prof_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(free_prof_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(free_prof_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(free_dummy_cnt, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(free_real_cnt, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(first_dummy_idx, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(last_real_idx, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(real_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(dummy_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(io_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(prof_switch_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(pkt_overflow_count, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(other_overflow_count, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(tot_io_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(tot_spin_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(real_io_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(db_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(relevant_db_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(db_delay2, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(tot_prep_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(sched_install, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(ack_update, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(rtx_update, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(prof_update1_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(prof_update2_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(cwnd_update3_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(cwnd_update4_delay, MAX_HP_ARGS);

SME_DECL_STAT_ARR_EXTERN(nic_overflow_delay, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(bufenc, MAX_HP_ARGS);
SME_DECL_STAT_ARR_EXTERN(cpyenc, MAX_HP_ARGS);

extern uint64_t nic_overflow_start;
extern uint64_t nic_overflow_end;
extern uint32_t nic_overflow_bd_start;
extern uint32_t nic_overflow_pkt_start;
extern uint32_t nic_overflow_prep_start;

#endif /* __XSTATS_DECL_H__ */
