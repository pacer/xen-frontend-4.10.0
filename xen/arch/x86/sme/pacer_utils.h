/*
 * pacer_utils.h
 *
 * created on: Jan 19, 2019
 * author: aasthakm
 *
 * functions invoked as part of xen_pacer interrupt handler
 */

#ifndef __PACER_UTILS_H__
#define __PACER_UTILS_H__

#include "timerobj.h"
#include "xtxdata.h"
#include "xnicq_hdr.h"

long do_fetch_one_profile(void);

void populate_shared_pointers(profq_elem_t *gprof);
void unmap_shared_pointers(profq_elem_t *gprof);
void update_shared_pointers(profq_elem_t *gprof, uint32_t *seqno);
int prep_nic_slot(timer_obj_t *tobj, int cons_idx, xtxdata_t *x_txdata,
    xnic_txintq_t *xnic_txintq);
void do_set_next_timer(timer_obj_t *tobj);
void do_set_next_timer_unlocked(timer_obj_t *tobj);

int prep_merged_nicq_slot(timer_obj_t *tobj, nic_txdata_t *nic_data_elem, int real,
    xtxdata_t *x_txdata, struct bnx2x_fp_txdata *gtxdata,
    xnic_txintq_t *xnic_txintq);

#endif /* __PACER_UTILS_H__ */
