#include <xen/lib.h>

#include "xconfig.h"
#include "xdebug.h"
#include "xstats.h"
#include "mem_trans.h"
#include "pacer_obj.h"
#include "sme_pkt.h"
#include "xprofq_hdr.h"
#include "xnicq_hdr.h"
#include "xprofile.h"
#include "pacer_utils.h"
#include "xstats_decl.h"

#if 0
extern timerq_t pac_timerq;
extern active_timerq_t active_timerq;
extern pacer_obj_t pac_obj;
extern xprofq_hdr_t guest_profq_hdr;
extern xprofq_freelist2_t guest_profq_fl;
extern xglobal_nicq_hdr_t guest_nicq;
extern xglobal_nicq_hdr_t guest_dummy_nicq;
#endif
extern sme_htable_t pmap_htbl;

#if 0
long
do_fetch_one_profile(void)
{
  int ret = 0;
  int earliest_idx = 0;
  int32_t pop_timerq_idx = 0;
  uint64_t next_ts = 0;
  timer_obj_t *new_tobj = NULL;
  profq_elem_t *prof = NULL;
  int nic_data_ptr_idx = 0;

  xprofile_t *xprof = NULL;
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
  s_time_t sched_delay = 0;
  unsigned long atq_flag = 0;

  SME_INIT_TIMER(active_lock_delay);

#if CONFIG_SYNC == SYNC_LOCKED
  ret = remove_locked(&guest_profq_hdr, &earliest_idx);
#else
  // CAS critical section with guest
  ret = remove_first(&guest_profq_hdr);
  earliest_idx = ret;
#endif

  if (!(earliest_idx >= 1
        && earliest_idx <= guest_profq_hdr.qsize - 2)) {
    if (smp_processor_id() != 0) {
      xprintk(LVL_DBG, "index out of range [1 - %ld] profq idx %d ret %d"
          " head %d tail %d head.nxt %d"
          , guest_profq_hdr.qsize - 2, earliest_idx, ret
          , atomic_read(&guest_profq_hdr.head)
          , atomic_read(&guest_profq_hdr.tail)
          , atomic_read(&(guest_profq_hdr.queue[0])->next)
          );
    }
    return -ENOENT;
  }

  prof = guest_profq_hdr.queue[earliest_idx];
  // get vaddr of various variables in guest pcm
  populate_shared_pointers(prof);

  // critical section with xen_pacer
  ret = pop_one_timer(&pac_timerq, &pop_timerq_idx);
  if (ret < 0)
    return ret;

  nic_data_ptr_idx = (earliest_idx-1) * NIC_DATA_QSIZE;
  new_tobj = &pac_timerq.queue[pop_timerq_idx];
#if CONFIG_NIC_QUEUE == CONFIG_NQ_STATIC
  timer_obj_set_profile(new_tobj, TT_PROF, prof, &pmap_htbl,
      &pac_obj.nic_txdata_ptr_arr[nic_data_ptr_idx]);
#else
  timer_obj_set_profile(new_tobj, TT_PROF, prof, &pmap_htbl, NULL);
#endif
  xprof = &new_tobj->xen_profp;
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);

  // I think we do not even need a lock here, since tobj is popped
  // from timerqueue, and not yet placed in active timerqueue.
  // critical section 0: xen profile
  ret = xprofile_pick_next_ts(&new_tobj->xen_profp, prof, &next_ts);
  // end of critical section 0

  /*
   * on first installation of a profile, it may already have to be paused
   * if cwm is 0. this may happen because previous profile sent out a lot
   * of pkts (in particular these pkts will be dummies), which have been
   * unacked yet. the unacked pkts are dummies because the fact that a new
   * profile is being installed implies a new request, which in turn implies
   * that the client received the previous response completely.
   */
  if (ret >= 0 || ret == -XP_PAUSE) {

    SME_START_TIMER(active_lock_delay);

    // critical section 1: active timerq
    spin_lock_irqsave(&active_timerq.active_lock, atq_flag);

    SME_END_TIMER(active_lock_delay);

    active_timerq_insert_idx_at_head(&active_timerq, pop_timerq_idx);
    // end of critical section 1

    spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

    if (ret >= 0) {
      timer_obj_set_timer(new_tobj, next_ts, ret, PACER_SPIN_THRESHOLD);
      sched_delay = NOW() - new_tobj->xen_profp.req_stime;
//      SME_ADD_COUNT_POINT(sched_install, sched_delay);
    }

    ret = 0;

    xprintk(LVL_DBG, "Picked T%d [%s %u, %s %u] #timers %d "
        "head %d => %d ret %d "
        "FL prod %d cons %d NIDX %d"
        , pop_timerq_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        , prof->num_timers
        , earliest_idx
        , atomic_read(&(guest_profq_hdr.queue[
            atomic_read(&pac_obj.profq_hdr_vaddr->u.sorted2.head)]->next))
        , ret
        , atomic_read(guest_profq_fl.prod_idx_p)
        , atomic_read(guest_profq_fl.cons_idx_p)
        , nic_data_ptr_idx
        );
  } else {
    xprintk(LVL_INFO, "Failed T%d G%d [%s %u, %s %u] CWM %d NXT %d P %d next %ld ret %d"
        , pop_timerq_idx, earliest_idx, x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port, prof->cwnd_watermark
        , prof->next_timer_idx, prof->num_timers, next_ts, ret);
    reset_timer_obj(new_tobj);
    push_one_timer(&pac_timerq, pop_timerq_idx);
    // failed to use the profile, atleast put it back into freelist
    // to avoid memory leak.
    put_one_profq_freelist2(&guest_profq_fl, earliest_idx);
  }

  return ret;
}
#endif

void
populate_shared_pointers(profq_elem_t *gprof)
{
  int profq_idx = 0;
  char g_out_ipstr[16];
  char g_dst_ipstr[16];

  if (!gprof)
    return;

  profq_idx = gprof->profq_idx;
  ipstr(gprof->conn_id.out_ip, g_out_ipstr);
  ipstr(gprof->conn_id.dst_ip, g_dst_ipstr);

#if CONFIG_DUMMY == DUMMY_OOB
  gprof->shared_seq_p = NULL;
  gprof->shared_dummy_out_p = NULL;
  gprof->shared_dummy_lsndtime_p = NULL;

  if (gprof->shared_seq_maddr) {
    gprof->shared_seq_p = get_vaddr_from_guest_maddr(gprof->shared_seq_maddr);
  }
  if (gprof->shared_dummy_out_maddr) {
    gprof->shared_dummy_out_p =
      get_vaddr_from_guest_maddr(gprof->shared_dummy_out_maddr);
  }
  if (gprof->shared_dummy_lsndtime_maddr) {
    gprof->shared_dummy_lsndtime_p =
      get_vaddr_from_guest_maddr(gprof->shared_dummy_lsndtime_maddr);
  }
#endif
  gprof->src_mac_p = get_vaddr_from_guest_maddr(gprof->src_mac_maddr);
  gprof->dst_mac_p = get_vaddr_from_guest_maddr(gprof->dst_mac_maddr);

  gprof->last_real_ack_p = get_vaddr_from_guest_maddr(gprof->last_real_ack_maddr);

#if !CONFIG_XEN_PACER_SLOTSHARE
#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  gprof->pcm_nicq_p = NULL;
  if (gprof->pcm_nicq_maddr)
    gprof->pcm_nicq_p = get_vaddr_from_guest_maddr(gprof->pcm_nicq_maddr);
#endif
#endif

#if 1
  {
  char s_macstr[ETH_ALEN*3], d_macstr[ETH_ALEN*3];
  macstr(gprof->src_mac_p, s_macstr, ETH_ALEN*3);
  macstr(gprof->dst_mac_p, d_macstr, ETH_ALEN*3);
  xprintk(LVL_DBG, "G%d [%s %u %s %u] seq %0lx %p dummy_out %0lx %p"
      "\ndummy_lsndtime %0lx %p smac %0lx %p %s dmac %0lx %p %s"
      , profq_idx
      , g_out_ipstr, gprof->conn_id.out_port, g_dst_ipstr, gprof->conn_id.dst_port
      , gprof->shared_seq_maddr, gprof->shared_seq_p
      , gprof->shared_dummy_out_maddr, gprof->shared_dummy_out_p
      , gprof->shared_dummy_lsndtime_maddr, gprof->shared_dummy_lsndtime_p
      , gprof->src_mac_maddr, gprof->src_mac_p, s_macstr
      , gprof->dst_mac_maddr, gprof->dst_mac_p, d_macstr
      );
  }
#endif
}

void
unmap_shared_pointers(profq_elem_t *gprof)
{
#if !CONFIG_XEN_PACER_SLOTSHARE
#if CONFIG_NIC_QUEUE == CONFIG_NQ_DYNAMIC
  if (gprof->pcm_nicq_p)
    unmap_domain_page_global(gprof->pcm_nicq_p);
#endif /* CONFIG_NIC_QUEUE */
#endif /* CONFIG_XEN_PACER_SLOTSHARE */

  if (gprof->last_real_ack_p)
    unmap_domain_page_global(gprof->last_real_ack_p);
  if (gprof->dst_mac_p)
    unmap_domain_page_global(gprof->dst_mac_p);
  if (gprof->src_mac_p)
    unmap_domain_page_global(gprof->src_mac_p);
  if (gprof->shared_dummy_lsndtime_p)
    unmap_domain_page_global(gprof->shared_dummy_lsndtime_p);
  if (gprof->shared_dummy_out_p)
    unmap_domain_page_global(gprof->shared_dummy_out_p);
  if (gprof->shared_seq_p)
    unmap_domain_page_global(gprof->shared_seq_p);
}

#if CONFIG_DUMMY == DUMMY_OOB
void
update_shared_pointers(profq_elem_t *gprof, uint32_t *seqno)
{
  uint32_t curr_seq;
  uint32_t curr_dummy_out;
  int ret = 0;

  if (!gprof || !seqno)
    return;

  if (!gprof->shared_seq_p || !gprof->shared_dummy_out_p
      || !gprof->shared_dummy_lsndtime_p) {
#if 0
    xprintk(LVL_DBG, "seq %0lx dummy_out %0lx dummy_lsndtime %0lx"
        , gprof->shared_seq_maddr, gprof->shared_dummy_out_maddr
        , gprof->shared_dummy_lsndtime_maddr);
    /*
     * on the initial handshake profile, we may still need to send
     * dummies after the initial synack. by then we should have the
     * tcp socket initialized in the guest, so we try to populate
     * the pointers one more time here.
     */
    populate_shared_pointers(gprof);

    if (!gprof->shared_seq_p || !gprof->shared_dummy_out_p
        || !gprof->shared_dummy_lsndtime_p)
#endif
      return;
  }

  do {
    curr_seq = *(gprof->shared_seq_p);
    ret = cmpxchg(gprof->shared_seq_p, curr_seq, curr_seq+TCP_MTU);
  } while (ret != curr_seq);

  *seqno = curr_seq;

  do {
    curr_dummy_out = *(gprof->shared_dummy_out_p);
    ret = cmpxchg(gprof->shared_dummy_out_p, curr_dummy_out, curr_dummy_out+1);
  } while (ret != curr_dummy_out);

  *(gprof->shared_dummy_lsndtime_p) = NOW();
}
#endif

#if !CONFIG_XEN_PACER_SLOTSHARE
#if 0
int
prep_nic_slot(timer_obj_t *tobj, int cons_idx, xtxdata_t *x_txdata,
    xnic_txintq_t *xnic_txintq)
{
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  nic_txdata_t *nic_data_elem = NULL;
  nic_txint_elem_t *nic_txint_elem = NULL;

  u16 pkt_prod, bd_prod;
  struct sw_tx_bd *tx_buf = NULL;
  struct eth_tx_start_bd *tx_start_bd, *first_bd;
  struct eth_tx_bd *tx_data_bd;
  struct eth_tx_parse_bd_e2 *pbd_e2 = NULL;
  int nbd;

  xprof = &tobj->xen_profp;
  gprof = tobj->profq_addr;

  nic_data_elem = guest_nicq.queue[cons_idx];

  pkt_prod = *(x_txdata->tx_pkt_prod_p);
  bd_prod = TX_BD(*(x_txdata->tx_bd_prod_p));
  tx_buf = x_txdata->tx_buf_ring[TX_BD(pkt_prod)];
  tx_start_bd = &(x_txdata->tx_desc_ring[bd_prod].start_bd);
  first_bd = tx_start_bd;
  tx_start_bd->bd_flags.as_bitfield = nic_data_elem->txbd_flags;
  tx_start_bd->general_data = nic_data_elem->txbd_general_data;
  tx_start_bd->vlan_or_ethertype = nic_data_elem->txbd_vlan_or_ethertype;
  tx_buf->first_bd = *(x_txdata->tx_bd_prod_p);
  tx_buf->flags = nic_data_elem->txbuf_flags;

  nbd = 2;
  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));

  pbd_e2 = &x_txdata->tx_desc_ring[bd_prod].parse_bd_e2;
  memset(pbd_e2, 0, sizeof(struct eth_tx_parse_bd_e2));

  bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.src_hi,
      &pbd_e2->data.mac_addr.src_mid, &pbd_e2->data.mac_addr.src_lo,
      nic_data_elem->s_mac);

  bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.dst_hi,
      &pbd_e2->data.mac_addr.dst_mid, &pbd_e2->data.mac_addr.dst_lo,
      nic_data_elem->d_mac);

  tx_start_bd->addr_hi = nic_data_elem->txbd_addr_hi;
  tx_start_bd->addr_lo = nic_data_elem->txbd_addr_lo;
  tx_start_bd->nbytes = nic_data_elem->txbd_nbytes;

  pbd_e2->parsing_data = nic_data_elem->pbd2_parsing_data;

  tx_data_bd = (struct eth_tx_bd *) tx_start_bd;

  first_bd->nbd = cpu_to_le16(nbd);

  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
  if (TX_BD_POFF(bd_prod) < nbd)
    nbd++;

  nic_txint_elem = xnic_txintq->queue_p[TX_BD(pkt_prod)];
  nic_txint_elem->skb_maddr = nic_data_elem->skb_maddr;

  *(x_txdata->tx_pkt_prod_p) = *(x_txdata->tx_pkt_prod_p) + 1;
  *(x_txdata->tx_bd_prod_p) = *(x_txdata->tx_bd_prod_p) + nbd;
  wmb();

  return nbd;
}
#endif

/*
 * only supports CONFIG_NIC_QUEUE=CONFIG_NQ_GLOBAL
 */
int
prep_merged_nicq_slot(timer_obj_t *tobj, nic_txdata_t *nic_data_elem, int real,
    xtxdata_t *x_txdata, struct bnx2x_fp_txdata *gtxdata, xnic_txintq_t *xnic_txintq)
{
  profq_elem_t *gprof = NULL;
  xprofile_t *xprof = NULL;
  nic_txint_elem_t *nic_txint_elem = NULL;

  u16 pkt_prod, bd_prod;
  struct sw_tx_bd *tx_buf = NULL;
  struct eth_tx_start_bd *tx_start_bd, *first_bd;
  struct eth_tx_bd *tx_data_bd;
  struct eth_tx_parse_bd_e2 *pbd_e2 = NULL;
  int nbd;

  xprof = &tobj->xen_profp;
  gprof = tobj->profq_addr;

//  pkt_prod = *(x_txdata->tx_pkt_prod_p);
//  bd_prod = TX_BD(*(x_txdata->tx_bd_prod_p));
  pkt_prod = gtxdata->tx_pkt_prod;
  bd_prod = TX_BD((gtxdata->tx_bd_prod));
  tx_buf = x_txdata->tx_buf_ring[TX_BD(pkt_prod)];
  tx_start_bd = &(x_txdata->tx_desc_ring[bd_prod].start_bd);
  first_bd = tx_start_bd;
  tx_start_bd->bd_flags.as_bitfield = nic_data_elem->txbd_flags;
  tx_start_bd->general_data = nic_data_elem->txbd_general_data;
  tx_start_bd->vlan_or_ethertype = nic_data_elem->txbd_vlan_or_ethertype;
//  tx_buf->first_bd = *(x_txdata->tx_bd_prod_p);
  tx_buf->first_bd = (gtxdata->tx_bd_prod);
  tx_buf->flags = nic_data_elem->txbuf_flags;

  nbd = 2;
  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));

  pbd_e2 = &x_txdata->tx_desc_ring[bd_prod].parse_bd_e2;
  memset(pbd_e2, 0, sizeof(struct eth_tx_parse_bd_e2));

  bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.src_hi,
      &pbd_e2->data.mac_addr.src_mid, &pbd_e2->data.mac_addr.src_lo,
      nic_data_elem->s_mac);

  bnx2x_set_fw_mac_addr(&pbd_e2->data.mac_addr.dst_hi,
      &pbd_e2->data.mac_addr.dst_mid, &pbd_e2->data.mac_addr.dst_lo,
      nic_data_elem->d_mac);

#if 0
  tx_start_bd->addr_hi = nic_data_elem->txbd_addr_hi;
  tx_start_bd->addr_lo = nic_data_elem->txbd_addr_lo;
#else
  *(u64 *) &tx_start_bd->addr_lo = *(u64 *) &nic_data_elem->txbd_addr_lo;
#endif

  tx_start_bd->nbytes = nic_data_elem->txbd_nbytes;

  pbd_e2->parsing_data = nic_data_elem->pbd2_parsing_data;

  tx_data_bd = (struct eth_tx_bd *) tx_start_bd;

  first_bd->nbd = cpu_to_le16(nbd);

  bd_prod = TX_BD(NEXT_TX_IDX(bd_prod));
  if (TX_BD_POFF(bd_prod) < nbd)
    nbd++;

  nic_txint_elem = xnic_txintq->queue_p[TX_BD(pkt_prod)];
  nic_txint_elem->skb_maddr = nic_data_elem->skb_maddr;
  nic_txint_elem->is_real = real;

//  *(x_txdata->tx_pkt_prod_p) = *(x_txdata->tx_pkt_prod_p) + 1;
//  *(x_txdata->tx_bd_prod_p) = *(x_txdata->tx_bd_prod_p) + nbd;
  gtxdata->tx_pkt_prod = gtxdata->tx_pkt_prod + 1;
  gtxdata->tx_bd_prod = gtxdata->tx_bd_prod + nbd;
  wmb();

  return nbd;
}
#endif /* CONFIG_XEN_PACER_SLOTSHARE */

#if 0
void
do_set_next_timer(timer_obj_t *tobj)
{
  int ret = 0;
  uint64_t next_ts = 0;
  xprofile_t *xprof = NULL;
  profq_elem_t *prof = NULL;
  unsigned long atq_flag = 0, xprof_flag = 0;
  int32_t push_timerq_idx = 0;
  int guest_prof_slot_idx = 0;

#if SME_DEBUG_LVL <= LVL_DBG
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
#endif

  SME_INIT_TIMER(active_lock_delay);
  SME_INIT_TIMER(timer_lock_delay);

  SME_START_TIMER(active_lock_delay);

  // critical section 0: active timerq must be locked before accessing active tobj
  spin_lock_irqsave(&active_timerq.active_lock, atq_flag);

  SME_END_TIMER(active_lock_delay);

  SME_START_TIMER(timer_lock_delay);

  // critical section 1: xen profile
  spin_lock_irqsave(&(tobj->xen_profp.timers_lock), xprof_flag);

  SME_END_TIMER(timer_lock_delay);

  if (tobj->profq_addr->state == PSTATE_XEN_COMPL && tobj->timer_idx == -1) {
    // on spurious timer after active_timerq_free_profile free this profile
    // and timer obj
    spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

    // end of critical section 0
    spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

    return;
  }

  push_timerq_idx = tobj - &pac_timerq.queue[0];

  xprof = &tobj->xen_profp;
  prof = tobj->profq_addr;

#if SME_DEBUG_LVL <= LVL_DBG
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  guest_prof_slot_idx = tobj->profq_addr->profq_idx;
#endif

  ret = xprofile_pick_next_ts_unlocked(&tobj->xen_profp,
      tobj->profq_addr, &next_ts);
  if (ret >= 0) {
    // profile not completed yet. we do not have to move it
    // from active timerq to timerq yet.
    timer_obj_set_timer(tobj, next_ts, ret, PACER_SPIN_THRESHOLD);

#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_DBG, "T(%d:%d) G%d xprof [%s %u, %s %u] UNLOCK REG"
        , push_timerq_idx, tobj->timer_idx, gprof->profq_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        );
#endif

    // end of critical section 1
    spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

    // end of critical section 0
    spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

  } else if (ret == -XP_COMPL /* || ret == -XP_PAUSE */) { //2nd cond. temp.
    // free xprofile->timers array
#if CONFIG_PROF_FREE == PROF_FREE_XEN
    cleanup_xprofile_unlocked(&tobj->xen_profp);
    atomic_set(&prof->is_paused, 0);
#endif

    prof->state = PSTATE_XEN_COMPL;
    guest_prof_slot_idx = tobj->profq_addr->profq_idx;

#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_INFO, "T(%d:%d) G%d xprof [%s %u, %s %u] "
      "FL prod %d cons %d reset #timers %d real %d dummy %d paused %d"
      , push_timerq_idx, tobj->timer_idx, gprof->profq_idx
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , atomic_read(guest_profq_fl.prod_idx_p)
      , atomic_read(guest_profq_fl.cons_idx_p)
      , prof->num_timers, atomic_read(&prof->real_counter)
      , prof->dummy_counter, atomic_read(&prof->is_paused));
#endif

#if CONFIG_PROF_FREE == PROF_FREE_XEN
    ret = active_timerq_remove_idx_unlocked(&active_timerq, push_timerq_idx);
    if (ret >= 0) {
      // reset timer obj, especially prof and xprof pointers
      // before returning it to the timer pool.
      //reset_timer_obj(tobj);
      tobj->true_expires = -1;
      tobj->profq_addr = NULL;
      tobj->timer_idx = -1;
      tobj->type = 0;

//      xprintk(LVL_DBG, "T(%d:%d) idx %d UNLOCK FREE"
//          , push_timerq_idx, tobj->timer_idx, guest_prof_slot_idx
//          );

      // end of critical section 1
      spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

      // end of critical section 0
      spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

      // critical section 3: cmpxchg in timerq
      push_one_timer(&pac_timerq, push_timerq_idx);
    } else {
//      xprintk(LVL_DBG, "T(%d:%d) idx %d UNLOCK FREE ERR %d"
//          , push_timerq_idx, tobj->timer_idx, guest_prof_slot_idx, ret
//          );

      // end of critical section 1
      spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

      // end of critical section 0
      spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

    }

    put_one_profq_freelist2(&guest_profq_fl, guest_prof_slot_idx);
#else

    // end of critical section 1
    spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

    // end of critical section 0
    spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

#endif
  } else {
#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_DBG, "T(%d:%d) G%d xprof[%s %u, %s %u] "
        "#timers %d nxt %d cwm %d real %d dummy %d paused %d max_seqno %u"
//        "NIC DB %u => %u bd prod %u pkt prod %u bd cons %u pkt cons %u "
//        "DUMMY DB %u => %u bd prod %u pkt prod %u bd cons %u pkt cons %u "
        , push_timerq_idx, tobj->timer_idx, guest_prof_slot_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port, prof->num_timers
        , prof->next_timer_idx, prof->cwnd_watermark
        , atomic_read(&prof->real_counter), prof->dummy_counter
        , atomic_read(&prof->is_paused), prof->max_seqno
//        , old_prod, new_prod, txdata->tx_bd_prod, txdata->tx_pkt_prod
//        , txdata->tx_bd_cons, txdata->tx_pkt_cons
//        , old_dummy_prod, new_dummy_prod
//        , dummy_txdata->tx_bd_prod, dummy_txdata->tx_pkt_prod
//        , dummy_txdata->tx_bd_cons, dummy_txdata->tx_pkt_cons
        );
#endif

    spin_unlock_irqrestore(&(tobj->xen_profp.timers_lock), xprof_flag);

    // end of critical section 0
    spin_unlock_irqrestore(&active_timerq.active_lock, atq_flag);

  }

}

void
do_set_next_timer_unlocked(timer_obj_t *tobj)
{
  int ret = 0;
  uint64_t next_ts = 0;
  xprofile_t *xprof = NULL;
  profq_elem_t *prof = NULL;
  int32_t push_timerq_idx = 0;
  int guest_prof_slot_idx = 0;

#if SME_DEBUG_LVL <= LVL_DBG
  char x_out_ipstr[16];
  char x_dst_ipstr[16];
#endif

  if (tobj->profq_addr->state == PSTATE_XEN_COMPL && tobj->timer_idx == -1) {
    // on spurious timer after active_timerq_free_profile free this profile
    // and timer obj
    return;
  }

  push_timerq_idx = tobj - &pac_timerq.queue[0];

  xprof = &tobj->xen_profp;
  prof = tobj->profq_addr;

#if SME_DEBUG_LVL <= LVL_DBG
  ipstr(xprof->conn_id.out_ip, x_out_ipstr);
  ipstr(xprof->conn_id.dst_ip, x_dst_ipstr);
  guest_prof_slot_idx = tobj->profq_addr->profq_idx;
#endif

  ret = xprofile_pick_next_ts_unlocked(&tobj->xen_profp,
      tobj->profq_addr, &next_ts);
  if (ret >= 0) {
    // profile not completed yet. we do not have to move it
    // from active timerq to timerq yet.
    timer_obj_set_timer(tobj, next_ts, ret, PACER_SPIN_THRESHOLD);

#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_DBG, "T(%d:%d) G%d xprof [%s %u, %s %u] UNLOCK REG"
        , push_timerq_idx, tobj->timer_idx, gprof->profq_idx
        , x_out_ipstr, xprof->conn_id.out_port
        , x_dst_ipstr, xprof->conn_id.dst_port
        );
#endif

  } else if (ret == -XP_COMPL /* || ret == -XP_PAUSE */) { //2nd cond. temp.
    // free xprofile->timers array
#if CONFIG_PROF_FREE == PROF_FREE_XEN
    cleanup_xprofile_unlocked(&tobj->xen_profp);
    atomic_set(&prof->is_paused, 0);
#endif

    prof->state = PSTATE_XEN_COMPL;
    guest_prof_slot_idx = tobj->profq_addr->profq_idx;

#if SME_DEBUG_LVL <= LVL_DBG
    xprintk(LVL_INFO, "T(%d:%d) G%d xprof [%s %u, %s %u] "
      "FL prod %d cons %d reset #timers %d real %d dummy %d paused %d"
      , push_timerq_idx, tobj->timer_idx, gprof->profq_idx
      , x_out_ipstr, xprof->conn_id.out_port, x_dst_ipstr, xprof->conn_id.dst_port
      , atomic_read(guest_profq_fl.prod_idx_p)
      , atomic_read(guest_profq_fl.cons_idx_p)
      , prof->num_timers, atomic_read(&prof->real_counter)
      , prof->dummy_counter, atomic_read(&prof->is_paused));
#endif

#if CONFIG_PROF_FREE == PROF_FREE_XEN
    ret = active_timerq_remove_idx_unlocked(&active_timerq, push_timerq_idx);
    if (ret >= 0) {
      // reset timer obj, especially prof and xprof pointers
      // before returning it to the timer pool.
      //reset_timer_obj(tobj);
      tobj->true_expires = -1;
      tobj->profq_addr = NULL;
      tobj->timer_idx = -1;
      tobj->type = 0;

//      xprintk(LVL_DBG, "T(%d:%d) idx %d UNLOCK FREE"
//          , push_timerq_idx, tobj->timer_idx, guest_prof_slot_idx
//          );

      // critical section 3: cmpxchg in timerq
      push_one_timer(&pac_timerq, push_timerq_idx);
    }

    put_one_profq_freelist2(&guest_profq_fl, guest_prof_slot_idx);
#endif
  }
}
#endif

