#ifndef __OPENSSL_WRAP_H__
#define __OPENSSL_WRAP_H__

#include "openssl_st.h"

typedef struct ctr_state {
  unsigned char iv[16];
  unsigned int num;
  unsigned char ecount[16];
  unsigned char key[32];
} ctr_state_t;

static inline void init_ctr(ctr_state_t *state,
    unsigned char *key, unsigned char *iv)
{
  state->num = 0;
  memset(state->ecount, 0, 16);
  memset(state->iv + 8, 0, 8);
  memcpy(state->iv, iv, 8);
  memcpy(state->key, key, 32);
}

extern int init_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p,
    ctr_state_t *e_state, ctr_state_t *d_state,
    unsigned char *key, unsigned char *iv);
extern void reset_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p);
extern void free_aes(EVP_CIPHER_CTX **e_ctx_p, EVP_CIPHER_CTX **d_ctx_p);
extern int do_openssl_enc(EVP_CIPHER_CTX *e_ctx, ctr_state_t *state,
    unsigned char *plaintext, int plaintext_len, unsigned char *ciphertext);
extern int do_openssl_dec(EVP_CIPHER_CTX *d_ctx, ctr_state_t *state,
    unsigned char *ciphertext, int ciphertext_len, unsigned char *plaintext);

#endif /* __OPENSSL_WRAP_H__ */
