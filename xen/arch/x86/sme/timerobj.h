/*
 * timerobj.h
 *
 * created on: Mar 22, 2018
 * author: aasthakm
 *
 * objects for pacer timer interrupt handler
 */

#ifndef __TIMEROBJ_H__
#define __TIMEROBJ_H__

#include <xen/time.h>
#include <xen/timer.h>

#include "sme_guest.h"
#include "xprofile.h"
#include "xprofq_hdr.h"
#include "ptimer.h"

enum {
  TT_PROF = 0,
  TT_WAKEUP,
  TT_PROF_UPDATE,
  MAX_TT,
};

typedef struct timer_obj {
  struct timer xtimer;
  ptimer_t ptimer;
  /*
   * spin until this time after the timer expires
   */
  s_time_t true_expires;
  nic_txdata_t **nic_txdata_ptr_arr;
  /*
   * xen-mapped virtual address of the slot in profile queue
   * where profile ID or expiry time may have been updated
   */
  profq_elem_t *profq_addr;
  /*
   * pointer to the instantiated profile
   * in the profile list in the hypervisor
   */
  xprofile_t xen_profp;
  /*
   * idx into the profile's timer array
   */
  int16_t timer_idx;
  /*
   * timer type
   */
  uint8_t type;
  /*
   * link into timer list
   */
  atomic_t next;
} timer_obj_t;

void timer_obj_set_profile(timer_obj_t *tobj, uint8_t type, profq_elem_t *prof,
    sme_htable_t *pmap_htbl, nic_txdata_t **nic_txdata_ptr_arr);
void timer_obj_set_timer(timer_obj_t *tobj, uint64_t true_expires,
    int16_t timer_idx, s_time_t early_delta);
void timer_obj_set_timestamp(timer_obj_t *tobj, uint64_t true_expires,
    int16_t timer_idx, ptimer_heap_t *pth);
void reset_timer_obj(timer_obj_t *tobj);

// freelist of timers
typedef struct timerq {
  atomic_t first;
  int32_t qsize;
  timer_obj_t *queue;
} timerq_t;

int init_timerq(timerq_t *tq, int qsize, void (*timer_fn) (void *));
int cleanup_timerq(ptimer_heap_t *pth, timerq_t *tq);
int pop_one_timer(timerq_t *tq, int32_t *idx);
int push_one_timer(timerq_t *tq, int32_t idx);

typedef struct active_timerq {
  atomic_t first;
  spinlock_t active_lock;
  // read-only pointer to the queue in timerq
  timer_obj_t *queue;
} active_timerq_t;

void init_active_timerq(active_timerq_t *atq, timer_obj_t *queue);
void cleanup_active_timerq(ptimer_heap_t *pth, active_timerq_t *atq, timerq_t *tq);
int active_timerq_insert_idx_at_head(active_timerq_t *atq, int32_t tobj_idx);
int active_timerq_remove_idx_unlocked(active_timerq_t *atq, int32_t rm_idx);
int active_timerq_remove_idx(active_timerq_t *atq, int32_t rm_idx);
int active_timerq_update_profile(void *pobj_p, active_timerq_t *atq, timerq_t *tq,
    profq_elem_t *prof, sme_htable_t *pmap_htbl, int prof_extn,
    s_time_t epoch, ptimer_heap_t *pth);
int active_timerq_update_cwnd(void *pobj_p, active_timerq_t *atq, timerq_t *tq,
    profq_elem_t *gprof, int cmd, s_time_t epoch, ptimer_heap_t *pth);
int active_timerq_free_profile(active_timerq_t *atq, profq_elem_t *gprof,
    xprofq_hdr_t *guest_profq_hdr, xprofq_freelist2_t *guest_profq_fl,
    timerq_t *pac_timerq);

#endif /* __TIMEROBJ_H__ */
