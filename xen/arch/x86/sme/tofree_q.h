/*
 * tofreeq.h
 *
 * created on: Jan 20, 2018
 * author: aasthakm
 *
 * queue to hold timeseries data to be printed at rmmod
 */

#ifndef __TOFREEQ_H__
#define __TOFREEQ_H__

#include "xconfig.h"
#include "xdebug.h"
#include "sme_guest.h"
#include <xen/lib.h>
#include <xen/spinlock.h>

typedef struct tofreeq_elem {
  int profq_idx;
} tofreeq_elem_t;

typedef struct tofreeq {
  u32 size;
  u32 elem_size;
  u32 prod_idx;
  u32 cons_idx;
  tofreeq_elem_t *arr;
  spinlock_t lock;
} tofreeq_t;

int init_tofreeq(tofreeq_t *tofreeq, int size);
void cleanup_tofreeq(tofreeq_t *tofreeq);
int put_tofreeq(tofreeq_t *tofreeq, tofreeq_elem_t *v);
int get_tofreeq(tofreeq_t *tofreeq, tofreeq_elem_t *v);
void print_tofreeq(tofreeq_t *tofreeq);

static inline int tofreeq_empty(tofreeq_t *tofreeq)
{
  return (tofreeq->cons_idx == tofreeq->prod_idx);
}

static inline int tofreeq_full(tofreeq_t *tofreeq)
{
  return ((tofreeq->prod_idx % tofreeq->size == tofreeq->cons_idx % tofreeq->size)
      && (tofreeq->prod_idx / tofreeq->size != tofreeq->cons_idx / tofreeq->size));
}

typedef struct prof_update_q {
  int size;
  uint32_t prod_idx;
  uint32_t cons_idx;
  prof_update_arg_t *arr;
  spinlock_t lock;
} prof_update_q_t;

int init_prof_update_q(prof_update_q_t *q, int size);
void cleanup_prof_update_q(prof_update_q_t *q);
int put_prof_update_q(prof_update_q_t *q, prof_update_arg_t *v);
int get_prof_update_q(prof_update_q_t *q, prof_update_arg_t *v);

static inline int prof_update_q_empty(prof_update_q_t *q)
{
  return (q->cons_idx == q->prod_idx);
}

static inline int prof_update_q_full(prof_update_q_t *q)
{
  if (q->size == 0) {
    xprintk(LVL_EXP, "updateQ %p arr %p prod %d cons %d size %d"
        , q, q->arr, q->prod_idx, q->cons_idx, q->size
        );
    return 1;
  }

  return ((q->prod_idx % q->size == q->cons_idx % q->size)
      && (q->prod_idx / q->size != q->cons_idx / q->size));
}

#endif /* __TOFREEQ_H__  */
