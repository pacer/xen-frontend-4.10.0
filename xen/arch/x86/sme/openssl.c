#include "openssl_st.h"

//#include <stdint.h>
//#include <string.h>

typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;

unsigned int OPENSSL_ia32cap_P[4];

int aesni_set_encrypt_key(const unsigned char *userKey, int bits, AES_KEY *key);
int aesni_set_decrypt_key(const unsigned char *userKey, int bits, AES_KEY *key);
void aesni_encrypt(const unsigned char *in, unsigned char *out,
    const AES_KEY *key);
void aesni_decrypt(const unsigned char *in, unsigned char *out,
    const AES_KEY *key);
void aesni_ctr32_encrypt_blocks(const unsigned char *in, unsigned char *out,
    size_t blocks, const void *key, const unsigned char *ivec);

#define EVP_CIPHER_mode(e)  (EVP_CIPHER_flags(e) & EVP_CIPH_MODE)

unsigned long EVP_CIPHER_flags(EVP_CIPHER *cipher)
{
  return cipher->flags;
}

#define EVP_CIPHER_CTX_mode(c)  EVP_CIPHER_mode(EVP_CIPHER_CTX_cipher(c))

EVP_CIPHER_CTX *EVP_CIPHER_CTX_new(void)
{
  return OPENSSL_zalloc(EVP_CIPHER_CTX);
}

int EVP_CIPHER_CTX_reset(EVP_CIPHER_CTX *c)
{
  if (c == NULL)
    return 1;

  if (c->cipher != NULL) {
    if (c->cipher->cleanup && !c->cipher->cleanup(c))
      return 0;
    if (c->cipher_data && c->cipher->ctx_size)
      OPENSSL_cleanse(c->cipher_data, c->cipher->ctx_size);
  }
  OPENSSL_free(c->cipher_data);
//#ifndef OPENSSL_NO_ENGINE
//  ENGINE_finish(c->engine);
//#endif
  memset(c, 0, sizeof(*c));
  return 1;
}

int EVP_CIPHER_CTX_reset_shallow(EVP_CIPHER_CTX *c)
{
  if (c == NULL)
    return 1;

  if (c->cipher != NULL) {
    if (c->cipher->cleanup && !c->cipher->cleanup(c))
      return 0;
    if (c->cipher_data && c->cipher->ctx_size)
      OPENSSL_cleanse(c->cipher_data, c->cipher->ctx_size);
  }
  return 1;
}

void EVP_CIPHER_CTX_free(EVP_CIPHER_CTX *ctx)
{
  EVP_CIPHER_CTX_reset(ctx);
  OPENSSL_free(ctx);
}

EVP_CIPHER *EVP_CIPHER_CTX_cipher(EVP_CIPHER_CTX *ctx)
{
  return ctx->cipher;
}

int EVP_CIPHER_CTX_key_length(EVP_CIPHER_CTX *ctx)
{
  return ctx->key_len;
}

int EVP_CIPHER_CTX_iv_length(EVP_CIPHER_CTX *ctx)
{
  return ctx->cipher->iv_len;
}

int EVP_CIPHER_CTX_test_flags(EVP_CIPHER_CTX *ctx, int flags)
{
  return (ctx->flags & flags);
}

void *EVP_CIPHER_CTX_get_cipher_data(EVP_CIPHER_CTX *ctx)
{
  return ctx->cipher_data;
}

int EVP_CIPHER_CTX_num(EVP_CIPHER_CTX *ctx)
{
  return ctx->num;
}

void EVP_CIPHER_CTX_set_num(EVP_CIPHER_CTX *ctx, int num)
{
  ctx->num = num;
}

unsigned char *EVP_CIPHER_CTX_iv_noconst(EVP_CIPHER_CTX *ctx)
{
  return ctx->iv;
}

unsigned char *EVP_CIPHER_CTX_buf_noconst(EVP_CIPHER_CTX *ctx)
{
  return ctx->buf;
}

int EVP_CIPHER_CTX_ctrl(EVP_CIPHER_CTX *ctx, int type, int arg, void *ptr)
{
  int ret;

  if (!ctx->cipher) {
    EVPerr(EVP_F_EVP_CIPHER_CTX_CTRL, EVP_R_NO_CIPHER_SET);
    return 0;
  }

  if (!ctx->cipher->ctrl) {
    EVPerr(EVP_F_EVP_CIPHER_CTX_CTRL, EVP_R_CTRL_NOT_IMPLEMENTED);
    return 0;
  }

  ret = ctx->cipher->ctrl(ctx, type, arg, ptr);
  if (ret == -1) {
    EVPerr(EVP_F_EVP_CIPHER_CTX_CTRL, EVP_R_CTRL_OPERATION_NOT_IMPLEMENTED);
    return 0;
  }

  return ret;
}

int EVP_CipherInit_ex(EVP_CIPHER_CTX *ctx, EVP_CIPHER *cipher,
    ENGINE *impl, unsigned char *key, unsigned char *iv, int enc)
{
  if (enc == -1)
    enc = ctx->encrypt;
  else {
    if (enc)
      enc = 1;
    ctx->encrypt = enc;
  }
#ifndef OPENSSL_NO_ENGINE
#if 0
    if (ctx->engine && ctx->cipher
        && (cipher == NULL || cipher->nid == ctx->cipher->nid))
      goto skip_to_init;
#endif
#endif
  if (cipher) {
    if (ctx->cipher) {
      unsigned long flags = ctx->flags;
      EVP_CIPHER_CTX_reset(ctx);
      ctx->encrypt = enc;
      ctx->flags = flags;
    }
#ifndef OPENSSL_NO_ENGINE
#if 0
    if (impl) {
      if (!ENGINE_init(impl)) {
        EVPerr(EVP_F_EVP_CIPHERINIT_EX, EVP_R_INITIALIZATION_ERROR);
        return 0;
      }
    } else
      impl = ENGINE_get_cipher_engine(cipher->nid);
    if (impl) {
      const EVP_CIPHER *c = ENGINE_get_cipher(impl, cipher->nid);
      if (!c) {
        EVPerr(EVP_F_EVP_CIPHERINIT_EX, EVP_R_INITIALIZATION_ERROR);
        return 0;
      }

      cipher = c;
      ctx->engine = impl;
    } else
      ctx->engine = NULL;
#endif
#endif

    ctx->cipher = cipher;
    if (ctx->cipher->ctx_size) {
      ctx->cipher_data = OPENSSL_zalloc_array(char, ctx->cipher->ctx_size);
      if (ctx->cipher_data == NULL) {
        ctx->cipher = NULL;
        EVPerr(EVP_F_EVP_CIPHERINIT_EX, ERR_R_MALLOC_FAILURE);
        return 0;
      }
    } else {
      ctx->cipher_data = NULL;
    }
    ctx->key_len = cipher->key_len;
    ctx->flags &= EVP_CIPHER_CTX_FLAG_WRAP_ALLOW;
    if (ctx->cipher->flags & EVP_CIPH_CTRL_INIT) {
      if (!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_INIT, 0, NULL)) {
        ctx->cipher = NULL;
        EVPerr(EVP_F_EVP_CIPHERINIT_EX, EVP_R_INITIALIZATION_ERROR);
        return 0;
      }
    }
  } else if (!ctx->cipher) {
      EVPerr(EVP_F_EVP_CIPHERINIT_EX, EVP_R_NO_CIPHER_SET);
      return 0;
  }
#ifndef OPENSSL_NO_ENGINE
#if 0
skip_to_init:
#endif
#endif
  OPENSSL_assert(ctx->cipher->block_size == 1
      || ctx->cipher->block_size == 8
      || ctx->cipher->block_size == 16);

  if (!(ctx->flags & EVP_CIPHER_CTX_FLAG_WRAP_ALLOW)
      && EVP_CIPHER_CTX_mode(ctx) == EVP_CIPH_WRAP_MODE) {
    EVPerr(EVP_F_EVP_CIPHERINIT_EX, EVP_R_WRAP_MODE_NOT_ALLOWED);
    return 0;
  }

  if (!(EVP_CIPHER_flags(EVP_CIPHER_CTX_cipher(ctx)) & EVP_CIPH_CUSTOM_IV)) {
    switch (EVP_CIPHER_CTX_mode(ctx)) {
      case EVP_CIPH_CTR_MODE:
        ctx->num = 0;
        if (iv)
          memcpy(ctx->iv, iv, EVP_CIPHER_CTX_iv_length(ctx));
        break;

      default:
        return 0;
    }
  }

  if (key || (ctx->cipher->flags & EVP_CIPH_ALWAYS_CALL_INIT)) {
    if (!ctx->cipher->init(ctx, key, iv, enc))
      return 0;
  }
  ctx->buf_len = 0;
  ctx->final_used = 0;
  ctx->block_mask = ctx->cipher->block_size - 1;
  return 1;
}

int EVP_EncryptInit_ex(EVP_CIPHER_CTX *ctx, EVP_CIPHER *cipher,
    ENGINE *impl, unsigned char *key, unsigned char *iv)
{
  return EVP_CipherInit_ex(ctx, cipher, impl, key, iv, 1);
}

int EVP_DecryptInit_ex(EVP_CIPHER_CTX *ctx, EVP_CIPHER *cipher,
    ENGINE *impl, unsigned char *key, unsigned char *iv)
{
  return EVP_CipherInit_ex(ctx, cipher, impl, key, iv, 0);
}

#define PTRDIFF_T uint64_t

int is_partially_overlapping(const void *ptr1, const void *ptr2, int len)
{
  PTRDIFF_T diff = (PTRDIFF_T)ptr1 - (PTRDIFF_T)ptr2;
  int overlapped = (len > 0) & (diff != 0) & ((diff < (PTRDIFF_T) len) |
                                              (diff > (0 - (PTRDIFF_T) len)));
  return overlapped;
}

static int evp_EncryptDecryptUpdate(EVP_CIPHER_CTX *ctx,
    unsigned char *out, int *outl, unsigned char *in, int inl)
{
  int i, j, bl, cmpl = inl;

  if (EVP_CIPHER_CTX_test_flags(ctx, EVP_CIPH_FLAG_LENGTH_BITS))
    cmpl = (cmpl + 7) / 8;

  bl = ctx->cipher->block_size;

  if (ctx->cipher->flags & EVP_CIPH_FLAG_CUSTOM_CIPHER) {
    if (bl == 1 && is_partially_overlapping(out, in, cmpl)) {
      EVPerr(EVP_F_EVP_ENCRYPTDECRYPTUPDATE, EVP_R_PARTIALLY_OVERLAPPING);
      return 0;
    }

    i = ctx->cipher->do_cipher(ctx, out, in, inl);
    if (i < 0)
      return 0;
    else
      *outl = i;

    return 1;
  }

  if (inl <= 0) {
    *outl = 0;
    return inl == 0;
  }
  if (is_partially_overlapping(out + ctx->buf_len, in, cmpl)) {
    EVPerr(EVP_F_EVP_ENCRYPTDECRYPTUPDATE, EVP_R_PARTIALLY_OVERLAPPING);
    return 0;
  }

  if (ctx->buf_len == 0 && (inl & (ctx->block_mask)) == 0) {
    if (ctx->cipher->do_cipher(ctx, out, in, inl)) {
      *outl = inl;
      return 1;
    } else {
      *outl = 0;
      return 0;
    }
  }

  i = ctx->buf_len;
  OPENSSL_assert(bl <= (int) sizeof(ctx->buf));
  if (i != 0) {
    if (bl - i > inl) {
      memcpy(&(ctx->buf[i]), in, inl);
      ctx->buf_len += inl;
      *outl = 0;
      return 1;
    } else {
      j = bl - i;
      memcpy(&(ctx->buf[i]), in, j);
      inl -= j;
      in += j;
      if (!ctx->cipher->do_cipher(ctx, out, ctx->buf, bl))
        return 0;
      out += bl;
      *outl = bl;
    }
  } else
    *outl = 0;
  i = inl & (bl - 1);
  inl -= i;
  if (inl > 0) {
    if (!ctx->cipher->do_cipher(ctx, out, in, inl))
      return 0;
    *outl += inl;
  }

  if (i != 0)
    memcpy(ctx->buf, &(in[inl]), i);
  ctx->buf_len = i;
  return 1;
}

int EVP_EncryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl,
    unsigned char *in, int inl)
{
  if (!ctx->encrypt) {
    EVPerr(EVP_F_EVP_ENCRYPTUPDATE, EVP_R_INVALID_OPERATION);
    return 0;
  }

  return evp_EncryptDecryptUpdate(ctx, out, outl, in, inl);
}

int EVP_EncryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl)
{
  int n, ret;
  unsigned int i, b, bl;

  if (!ctx->encrypt) {
    EVPerr(EVP_F_EVP_ENCRYPTFINAL_EX, EVP_R_INVALID_OPERATION);
    return 0;
  }

  if (ctx->cipher->flags & EVP_CIPH_FLAG_CUSTOM_CIPHER) {
    ret = ctx->cipher->do_cipher(ctx, out, NULL, 0);
    if (ret < 0)
      return 0;
    else
      *outl = ret;
    return 1;
  }

  b = ctx->cipher->block_size;
  OPENSSL_assert(b <= sizeof(ctx->buf));
  if (b == 1) {
    *outl = 0;
    return 1;
  }
  bl = ctx->buf_len;
  if (ctx->flags & EVP_CIPH_NO_PADDING) {
    if (bl) {
      EVPerr(EVP_F_EVP_ENCRYPTFINAL_EX, EVP_R_DATA_NOT_MULTIPLE_OF_BLOCK_LENGTH);
      return 0;
    }
    *outl = 0;
    return 1;
  }

  n = b - bl;
  for (i = bl; i < b; i++)
    ctx->buf[i] = n;
  ret = ctx->cipher->do_cipher(ctx, out, ctx->buf, b);
  if (ret)
    *outl = b;

  return ret;
}

int EVP_DecryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl,
    unsigned char *in, int inl)
{
  int fix_len, cmpl = inl;
  unsigned int b;

  if (ctx->encrypt) {
    EVPerr(EVP_F_EVP_DECRYPTUPDATE, EVP_R_INVALID_OPERATION);
    return 0;
  }

  b = ctx->cipher->block_size;

  if (EVP_CIPHER_CTX_test_flags(ctx, EVP_CIPH_FLAG_LENGTH_BITS))
    cmpl = (cmpl + 7) / 8;

  if (ctx->cipher->flags & EVP_CIPH_FLAG_CUSTOM_CIPHER) {
    if (b == 1 && is_partially_overlapping(out, in, cmpl)) {
      EVPerr(EVP_F_EVP_DECRYPTUPDATE, EVP_R_PARTIALLY_OVERLAPPING);
      return 0;
    }

    fix_len = ctx->cipher->do_cipher(ctx, out, in, inl);
    if (fix_len < 0) {
      *outl = 0;
      return 0;
    } else
      *outl = fix_len;
    return 1;
  }

  if (inl <= 0) {
    *outl = 0;
    return inl == 0;
  }

  if (ctx->flags & EVP_CIPH_NO_PADDING)
    return evp_EncryptDecryptUpdate(ctx, out, outl, in, inl);

  OPENSSL_assert(b <= sizeof(ctx->final));

  if (ctx->final_used) {
    if (((PTRDIFF_T) out == (PTRDIFF_T) in)
        || is_partially_overlapping(out, in, b)) {
      EVPerr(EVP_F_EVP_DECRYPTUPDATE, EVP_R_PARTIALLY_OVERLAPPING);
      return 0;
    }
    memcpy(out, ctx->final, b);
    out += b;
    fix_len = 1;
  } else
    fix_len = 0;

  if (!evp_EncryptDecryptUpdate(ctx, out, outl, in, inl))
    return 0;

  if (b > 1 && !ctx->buf_len) {
    *outl -= b;
    ctx->final_used = 1;
    memcpy(ctx->final, &outl[*outl], b);
  } else
    ctx->final_used = 0;

  if (fix_len)
    *outl += b;

  return 1;
}

int EVP_DecryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *out, int *outl)
{
  int i, n;
  unsigned int b;

  if (ctx->encrypt) {
    EVPerr(EVP_F_EVP_DECRYPTFINAL_EX, EVP_R_INVALID_OPERATION);
    return 0;
  }

  *outl = 0;

  if (ctx->cipher->flags & EVP_CIPH_FLAG_CUSTOM_CIPHER) {
    i = ctx->cipher->do_cipher(ctx, out, NULL, 0);
    if (i < 0)
      return 0;
    else
      *outl = i;
    return 1;
  }

  b = ctx->cipher->block_size;
  if (ctx->flags & EVP_CIPH_NO_PADDING) {
    if (ctx->buf_len) {
      EVPerr(EVP_F_EVP_DECRYPTFINAL_EX, EVP_R_DATA_NOT_MULTIPLE_OF_BLOCK_LENGTH);
      return 0;
    }
    *outl = 0;
    return 1;
  }
  if (b > 1) {
    if (ctx->buf_len || !ctx->final_used) {
      EVPerr(EVP_F_EVP_DECRYPTFINAL_EX, EVP_R_WRONG_FINAL_BLOCK_LENGTH);
      return 0;
    }
    OPENSSL_assert(b <= sizeof(ctx->final));

    n = ctx->final[b - 1];
    if (n == 0 || n > (int) b) {
      EVPerr(EVP_F_EVP_DECRYPTFINAL_EX, EVP_R_BAD_DECRYPT);
      return 0;
    }
    for (i = 0; i < n; i++) {
      if (ctx->final[--b] != n) {
        EVPerr(EVP_F_EVP_DECRYPTFINAL_EX, EVP_R_BAD_DECRYPT);
        return 0;
      }
    }
    n = ctx->cipher->block_size - n;
    for (i = 0; i < n; i++)
      out[i] = ctx->final[i];
    *outl = n;
  } else
    *outl = 0;

  return 1;
}

/*
 * ==============
 * AESNI/AES code
 * ==============
 */

#if 0
static const EVP_CIPHER aes_256_wrap = {
  NID_id_aes256_wrap,
  8, 32, 8, WRAP_FLAGS,
  aes_wrap_init_key, aes_wrap_cipher,
  NULL,
  sizeof(EVP_AES_WRAP_CTX),
  NULL, NULL, NULL, NULL
};
#endif

static int aesni_init_key(EVP_CIPHER_CTX *ctx, unsigned char *key,
    unsigned char *iv, int enc)
{
  int ret = 0, mode;
  EVP_AES_KEY *dat = EVP_C_DATA(EVP_AES_KEY,ctx);

  mode = EVP_CIPHER_CTX_mode(ctx);
  if ((mode == EVP_CIPH_ECB_MODE || mode == EVP_CIPH_CBC_MODE) && !enc) {
    // ignore for now
  } else {
    ret = aesni_set_encrypt_key(key, EVP_CIPHER_CTX_key_length(ctx) * 8,
        &dat->ks.ks);
    dat->block = (block128_f) aesni_encrypt;
    if (mode == EVP_CIPH_CBC_MODE)
      dat->stream.cbc = NULL; // ignore
    else if (mode == EVP_CIPH_CTR_MODE)
      dat->stream.ctr = (ctr128_f) aesni_ctr32_encrypt_blocks;
    else
      dat->stream.cbc = NULL;
  }

  if (ret < 0) {
    EVPerr(EVP_F_AESNI_INIT_KEY, EVP_R_AES_KEY_SETUP_FAILED);
    return 0;
  }

  return 1;
}

static void ctr96_inc(unsigned char *counter)
{
  u32 n = 12, c = 1;

  do {
    --n;
    c += counter[n];
    counter[n] = (u8) c;
    c >>= 8;
  } while (n);
}

void CRYPTO_ctr128_encrypt_ctr32(const unsigned char *in, unsigned char *out,
    size_t len, const void *key, unsigned char ivec[16],
    unsigned char ecount_buf[16], unsigned int *num, ctr128_f func)
{
  unsigned int n, ctr32;

  n = *num;

  while (n && len) {
    *(out++) = *(in++) ^ ecount_buf[n];
    --len;
    n = (n + 1) % 16;
  }

  ctr32 = GETU32(ivec + 12);
  while (len >= 16) {
    size_t blocks = len / 16;
    if (sizeof(size_t) > sizeof(unsigned int) && blocks > (1U << 28))
      blocks = (1U << 28);

    ctr32 += (u32)blocks;
    if (ctr32 < blocks) {
      blocks -= ctr32;
      ctr32 = 0;
    }
    (*func) (in, out, blocks, key, ivec);
    PUTU32(ivec + 12, ctr32);
    if (ctr32 == 0)
      ctr96_inc(ivec);
    blocks *= 16;
    len -= blocks;
    out += blocks;
    in += blocks;
  }

  if (len) {
    memset(ecount_buf, 0, 16);
    (*func) (ecount_buf, ecount_buf, 1, key, ivec);
    ++ctr32;
    PUTU32(ivec + 12, ctr32);
    if (ctr32 == 0)
      ctr96_inc(ivec);
    while (len--) {
      out[n] = in[n] ^ ecount_buf[n];
      ++n;
    }
  }

  *num = n;
}

static void CRYPTO_ctr128_encrypt(const unsigned char *in, unsigned char *out,
    size_t len, const void *key, unsigned char ivec[16],
    unsigned char ecount_buf[16], unsigned int *num, block128_f block)
{
//  unsigned int n;
//  size_t l = 0;
//
//  n = *num;
  // pending
}

static int aes_ctr_cipher(EVP_CIPHER_CTX *ctx, unsigned char *out,
    unsigned char *in, size_t len)
{
  unsigned int num = EVP_CIPHER_CTX_num(ctx);
  EVP_AES_KEY *dat = EVP_C_DATA(EVP_AES_KEY,ctx);
  if (dat->stream.ctr)
    CRYPTO_ctr128_encrypt_ctr32(in, out, len, &dat->ks,
        EVP_CIPHER_CTX_iv_noconst(ctx), EVP_CIPHER_CTX_buf_noconst(ctx),
        &num, dat->stream.ctr);
  else
    CRYPTO_ctr128_encrypt(in, out, len, &dat->ks,
        EVP_CIPHER_CTX_iv_noconst(ctx), EVP_CIPHER_CTX_buf_noconst(ctx),
        &num, dat->block);
  EVP_CIPHER_CTX_set_num(ctx, num);
  return 1;
}

#define aesni_ctr_cipher    aes_ctr_cipher
//static int aesni_ctr_cipher(EVP_CIPHER_CTX *ctx, unsigned char *out,
//    const unsigned char *in, size_t len);

#if 0
static int aes_init_key(EVP_CIPHER_CTX *ctx, const unsigned char *key,
    const unsigned char *iv, int enc)
{
  int ret, mode;
  EVP_AES_KEY *dat = EVP_C_DATA(EVP_AES_KEY,ctx);

  mode = EVP_CIPHER_CTX_mode(ctx);
  if (mode == EVP_CIPH_ECB_MODE || mode == EVP_CIPH_CBC_MODE && !enc) {
    {
      // ignore for now
      // ret = AES_set_decrypt_key(...);
    }
  } else {
    ret = AES_set_encrypt_key(key, EVP_CIPHER_CTX_key_length(ctx) * 8,
        &dat->ks.ks);
    dat->block = (block128_f) AES_encrypt;
    dat->stream.cbc = NULL;
    if (mode == EVP_CIPH_CTR_MODE)
      dat->stream.ctr = (ctr128_f) AES_ctr32_encrypt;
  }

  if (ret < 0) {
    EVPerr(EVP_F_AES_INIT_KEY, EVP_R_AES_KEY_SETUP_FAILED);
    return 0;
  }

  return 1;
}
#endif

#define BLOCK_CIPHER_generic(nid,keylen,blocksize,ivlen,nmode,mode,MODE,flags)  \
  EVP_CIPHER aesni_##keylen##_##mode = { \
    nid##_##keylen##_##nmode,blocksize,keylen/8,ivlen,  \
    flags|EVP_CIPH_##MODE##_MODE, \
    aesni_init_key, \
    aesni_##mode##_cipher,  \
    NULL, \
    sizeof(EVP_AES_KEY),  \
    NULL, NULL, NULL, NULL }; \
EVP_CIPHER *EVP_aes_##keylen##_##mode(void) \
{ return AESNI_CAPABLE ? &aesni_##keylen##_##mode : NULL; }
/*
//static const EVP_CIPHER_aes_##keylen##_##mode = { \
//  nid##_##keylen##_##nmode,blocksize,keylen/8,ivlen,  \
//  flags|EVP_CIPH_##MODE##_MODE, \
//  aes_init_key, \
//  aes_##mode##_cipher,  \
//  NULL, \
//  sizeof(EVP_AES_KEY),  \
//  NULL, NULL, NULL, NULL }; \
*/

#define BLOCK_CIPHER_generic_pack(nid, keylen, flags) \
  BLOCK_CIPHER_generic(nid,keylen,1,16,ctr,ctr,CTR,flags)

BLOCK_CIPHER_generic_pack(NID_aes, 256, 0)

