#ifndef __MEM_TRANS_H__
#define __MEM_TRANS_H__

#include <xen/guest_access.h>

#include "sme_guest.h"
#include "xdebug.h"

static inline void *
get_vaddr_from_guest_maddr(uint64_t maddr)
{
  mfn_t xmfn;
  void *pg, *vaddr;
  void *epg, *evaddr;

  xmfn = _mfn(PFN_DOWN(maddr));
  // checking existing mapping
  epg = mfn_to_virt(mfn_x(xmfn));
  evaddr = epg + (maddr & (PAGE_SIZE - 1));
  // potentially get new mapping
  pg = map_domain_page_global(xmfn);
  vaddr = pg + (maddr & (PAGE_SIZE - 1));
  if (evaddr != vaddr) {
    xprintk(LVL_EXP, "%pS maddr %0lx ev %p v %p", __builtin_return_address(0)
        , maddr, evaddr, vaddr);
    return NULL;
  }
  return vaddr;
}

static inline int
generate_vaddrs_from_mdesc(mem_desc_t *md_arr, int n_md, int sizeof_vobj,
    void **vaddr_ptr_arr, int max_velems)
{
  int velem_it = 0;
  int md_it = 0;
  int n_elems = 0;
  int elem_it = 0;
  uint64_t maddr_it = 0;

  for (md_it = 0; md_it < n_md; md_it++) {
    n_elems = md_arr[md_it].n_elems;
    maddr_it = md_arr[md_it].maddr;
    xprintk(LVL_DBG, "[%d/%d] st %lx velem_it %d n_pelem %d"
        , md_it, n_md, maddr_it, velem_it, n_elems
        );

    for (elem_it = 0; elem_it < n_elems; elem_it++) {
      vaddr_ptr_arr[velem_it] = get_vaddr_from_guest_maddr(maddr_it);
      maddr_it += sizeof_vobj;
      velem_it++;

      if (velem_it == max_velems && (md_it < n_md-1 || elem_it < n_elems-1))
        return -ENOMEM;
    }
  }

  return 0;
}

static inline int
generate_vaddrs_from_mdesc_palign(mem_desc_t *md_arr, int n_md, int sizeof_vobj,
    void **vaddr_ptr_arr, int max_velems)
{
  int velem_it = 0;
  int md_it = 0;
  int n_elems = 0;
  int elem_it = 0;
  uint64_t maddr_it = 0;
  uint64_t page_start_maddr = 0;
  int n_elem_per_page = PAGE_SIZE/sizeof_vobj;

  for (md_it = 0; md_it < n_md; md_it++) {
    n_elems = md_arr[md_it].n_elems;
    maddr_it = md_arr[md_it].maddr;
    page_start_maddr = maddr_it;
    xprintk(LVL_DBG, "[%d/%d] st %lx velem_it %d n_pelem %d sz elem %d #elems/page %d"
        , md_it, n_md, maddr_it, velem_it, n_elems, sizeof_vobj, n_elem_per_page
        );

    for (elem_it = 0; elem_it < n_elems; elem_it++) {
      vaddr_ptr_arr[velem_it] = get_vaddr_from_guest_maddr(maddr_it);
      // last element that can fit on the page, move to start of next contig. page
      if (elem_it % n_elem_per_page == n_elem_per_page-1) {
        page_start_maddr += PAGE_SIZE;
        maddr_it = page_start_maddr;
      } else {
        maddr_it += sizeof_vobj;
      }
      velem_it++;

      if (velem_it == max_velems && (md_it < n_md-1 || elem_it < n_elems-1))
        return -ENOMEM;
    }
  }

  return 0;
}

#endif /* __MEM_TRANS_H__ */
