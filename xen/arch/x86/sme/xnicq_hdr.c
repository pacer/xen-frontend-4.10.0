/*
 * xnicq_hdr.c
 *
 * created on: Nov 20, 2018
 * author: aasthakm
 *
 * xen local version of shared nicq_hdr and nic_freelist API
 */

#include <xen/guest_access.h>

#include "xconfig.h"
#include "xdebug.h"
#include "xnicq_hdr.h"
#include "mem_trans.h"

#define NIC_REF_MASK    (1 << 30)
#define NIC_REF_UNMASK  ~(1 << 30)

/*
 * ================
 * xglobal_nicq_hdr
 * ================
 */

static int
is_marked_reference(int idx)
{
  return (int) (idx & NIC_REF_MASK);
}

static int
get_unmarked_reference(int idx)
{
  return (idx & NIC_REF_UNMASK);
}

static int
get_marked_reference(int idx)
{
  return (idx | NIC_REF_MASK);
}

static int
nicq_search(nicq_hdr_t *nicq_hdr, xglobal_nicq_hdr_t *xgnicq_hdr, uint64_t tsc_key)
{
  int idx, idx_next, idx_next_um, idx_last_unmarked, idx_last_unmarked_next;
  int idx_tail;
  int ret = 0;
  nic_txdata_t *t_elem = NULL;
  int dbg_nxtnxt = 0;

  if (!nicq_hdr || !xgnicq_hdr) {
    xprintk(LVL_EXP, "BUG!!! nicq_hdr %p xgnicq_hdr %p tsc %ld"
        , nicq_hdr, xgnicq_hdr, tsc_key);
    return -5;
  }

  do {
    idx = atomic_read(&nicq_hdr->head);
    // head has already been removed from the pcm
    if (idx < 0 || idx >= GLOBAL_NIC_DATA_QSIZE)
      return -1;

    idx_next = atomic_read(&xgnicq_hdr->queue[idx]->next);
    // concurrently, head->next was changed before freeing pcm
    if (idx_next < 0 || idx >= GLOBAL_NIC_DATA_QSIZE)
      return -1;

    idx_tail = atomic_read(&nicq_hdr->tail);
    // tail has already been removed from the pcm
    if (idx_tail < 0 || idx >= GLOBAL_NIC_DATA_QSIZE)
      return -1;

    idx_last_unmarked = idx;
    idx_last_unmarked_next = idx_next;

    if (idx_next < 0 || idx_next >= GLOBAL_NIC_DATA_QSIZE) {
      dbg_nxtnxt = atomic_read(&xgnicq_hdr->queue[get_unmarked_reference(idx_next)]->next);
      xprintk(LVL_EXP, "head %d tail %d idx %d idx_next %d %d nxtnxt %d %d"
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next, get_unmarked_reference(idx_next)
          , dbg_nxtnxt, get_unmarked_reference(dbg_nxtnxt)
          );
      return -1;
    }
    t_elem = xgnicq_hdr->queue[get_unmarked_reference(idx_next)];
    if (t_elem == NULL) {
      xprintk(LVL_EXP, "head %d tail %d idx %d idx_next %d"
          , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
          , idx, idx_next
          );
    }

    while (((idx_next_um = get_unmarked_reference(idx_next)) !=
          atomic_read(&nicq_hdr->tail))
      && (is_marked_reference(atomic_read(&xgnicq_hdr->queue[idx_next_um]->next))
        || t_elem->tx_tsc < tsc_key)) {

      if (!is_marked_reference(idx_next)) {
        idx_last_unmarked = idx;
        idx_last_unmarked_next = idx_next;
      }

      idx = idx_next_um;
      idx_next = get_unmarked_reference(atomic_read(&xgnicq_hdr->queue[idx]->next));
      t_elem = xgnicq_hdr->queue[get_unmarked_reference(idx_next)];

    }

    if (!is_marked_reference(idx_next))
      return idx;

    ret = atomic_cmpxchg(&xgnicq_hdr->queue[idx_last_unmarked]->next,
        idx_last_unmarked_next, idx_next_um);
    if (ret == idx_last_unmarked_next)
      return idx_last_unmarked;

  } while (1);

}

int
nicq_remove(nicq_hdr_t *nicq_hdr, xglobal_nicq_hdr_t *xgnicq_hdr, uint64_t tsc_key, profq_elem_t *gprof)
{
  int idx, idx_next, idx_nextnext;
  int ret = 0;
  nic_txdata_t *r_elem = NULL;

  do {
    idx = nicq_search(nicq_hdr, xgnicq_hdr, tsc_key);
    if (idx < 0 || idx >= GLOBAL_NIC_DATA_QSIZE)
      return idx;

    idx_next = get_unmarked_reference(atomic_read(&xgnicq_hdr->queue[idx]->next));

    if (idx_next == atomic_read(&nicq_hdr->tail))
      return -1;

    if (idx_next < 0 || idx_next >= GLOBAL_NIC_DATA_QSIZE)
      return -1;

    idx_nextnext = get_unmarked_reference(
        atomic_read(&xgnicq_hdr->queue[idx_next]->next));
    ret = atomic_cmpxchg(&xgnicq_hdr->queue[idx_next]->next, idx_nextnext,
        get_marked_reference(idx_nextnext));
    if (ret == idx_nextnext) {
      if (idx_next == atomic_read(&nicq_hdr->head)) {
        xprintk(LVL_EXP, "PORT %u state %d G%d W %d N %d P %d T %d R %d D %d\n"
            "head %d tail %d idx %d idx.nxt %d idx_nxt %d"
            " idx_nxt.nxt %d idx_nxtnxt %d ret %d"
            , gprof->conn_id.dst_port, gprof->state, gprof->profq_idx
            , gprof->cwnd_watermark, gprof->next_timer_idx, gprof->num_timers
            , gprof->tail, atomic_read(&gprof->real_counter), gprof->dummy_counter
            , atomic_read(&nicq_hdr->head), atomic_read(&nicq_hdr->tail)
            , idx, atomic_read(&xgnicq_hdr->queue[idx]->next), idx_next
            , atomic_read(&xgnicq_hdr->queue[idx_next]->next), idx_nextnext, ret
            );
      }
      break;
    }

  } while (1);

  ret = atomic_cmpxchg(&xgnicq_hdr->queue[idx]->next, idx_next, idx_nextnext);
  if (ret == idx_next)
    return idx_next;

  r_elem = xgnicq_hdr->queue[idx];
  idx = nicq_search(nicq_hdr, xgnicq_hdr, r_elem->tx_tsc);

  return idx_next;
}

void
init_x_global_nicq_hdr(xglobal_nicq_hdr_t *xgnicq_hdr,
    global_nicq_hdr_t *global_nicq)
{
  int ret = 0;

  xgnicq_hdr->qsize = global_nicq->qsize;
  xgnicq_hdr->queue = xzalloc_array(nic_txdata_t *, xgnicq_hdr->qsize);

  ret = generate_vaddrs_from_mdesc_palign(global_nicq->md_arr, global_nicq->n_md,
      sizeof(nic_txdata_t), (void **) xgnicq_hdr->queue, xgnicq_hdr->qsize);
}

void
cleanup_x_global_nicq_hdr(xglobal_nicq_hdr_t *xgnicq_hdr)
{
  int i = 0;

  if (xgnicq_hdr->queue) {
    for (i = 0; i < xgnicq_hdr->qsize; i++) {
      if (xgnicq_hdr->queue[i])
        unmap_domain_page_global(xgnicq_hdr->queue[i]);
    }
    xfree(xgnicq_hdr->queue);
  }
}

/*
 * =============
 * xnic_freelist
 * =============
 */

void
init_x_nic_freelist(xnic_freelist_t *xnfl, nic_freelist_t *global_nfl)
{
  int ret = 0;

  xnfl->fl_size = global_nfl->fl_size;
  xnfl->nfl_arr_p = xzalloc_array(nic_fl_elem_t *, xnfl->fl_size);

  ret = generate_vaddrs_from_mdesc(global_nfl->md_arr,
      global_nfl->n_md, sizeof(nic_fl_elem_t),
      (void **) xnfl->nfl_arr_p, xnfl->fl_size);

  xnfl->prod_idx_p = &global_nfl->prod_idx;
  xnfl->cons_idx_p = &global_nfl->cons_idx;

  xprintk(LVL_DBG, "xnfl %p arr %p prodp %p consp %p #md %d size %d"
      , xnfl, xnfl->nfl_arr_p, xnfl->prod_idx_p, xnfl->cons_idx_p
      , global_nfl->n_md, global_nfl->fl_size);
}

void
cleanup_x_nic_freelist(xnic_freelist_t *xnfl)
{
  int i = 0;

  if (xnfl->nfl_arr_p) {
    for (i = 0; i < xnfl->fl_size; i++) {
      if (xnfl->nfl_arr_p[i])
        unmap_domain_page_global(xnfl->nfl_arr_p[i]);
    }
    xfree(xnfl->nfl_arr_p);
  }
}

int
put_one_nic_freelist(xnic_freelist_t *xnfl, nic_fl_elem_t *nelem)
{
  int prod_idx = 0;

  prod_idx = *(xnfl->prod_idx_p) % xnfl->fl_size;
  if (!xnfl->nfl_arr_p || !xnfl->nfl_arr_p[prod_idx]) {
    xprintk(LVL_EXP, "NULL PTR prod %u mod %d size %d cons %d arr %p arr[%d] %p nelem %p"
        , *(xnfl->prod_idx_p), prod_idx, xnfl->fl_size, *(xnfl->cons_idx_p)
        , xnfl->nfl_arr_p, prod_idx
        , (xnfl->nfl_arr_p ? xnfl->nfl_arr_p[prod_idx] : NULL)
        , nelem
        );
  }

  xnfl->nfl_arr_p[prod_idx]->offset = nelem->offset;

  *(xnfl->prod_idx_p) = *(xnfl->prod_idx_p) + 1;

  xprintk(LVL_DBG, "nfl_arr_p %p prod %u cons %u nelem %p idx %d"
      , xnfl->nfl_arr_p, *(xnfl->prod_idx_p), *(xnfl->cons_idx_p)
      , nelem, (nelem ? nelem->offset : -1));

  return 0;
}

/*
 * ===========
 * xnic_txintq
 * ===========
 */
void
init_x_nic_txintq(xnic_txintq_t *xnic_txintq, nic_txintq_t *guest_txintq)
{
  int ret = 0;

  xnic_txintq->qsize = guest_txintq->qsize;
  xnic_txintq->queue_p = xzalloc_array(nic_txint_elem_t *, xnic_txintq->qsize);

  ret = generate_vaddrs_from_mdesc_palign(guest_txintq->md_arr, guest_txintq->n_md,
      sizeof(nic_txint_elem_t), (void **) xnic_txintq->queue_p, xnic_txintq->qsize);
}

void
cleanup_x_nic_txintq(xnic_txintq_t *xnic_txintq)
{
  int i = 0;

  if (xnic_txintq->queue_p) {
    for (i = 0; i < xnic_txintq->qsize; i++) {
      if (xnic_txintq->queue_p[i])
        unmap_domain_page_global(xnic_txintq->queue_p[i]);
    }
    xfree(xnic_txintq->queue_p);
  }
}

